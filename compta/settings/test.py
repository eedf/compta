# Copyright 2023 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from .base import *  # NOQA

SECRET_KEY = 'test'
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]
BROKER_ENABLED = False
