#!/bin/bash

set -ex

echo "======== BEGIN `date` ========"

# Clear sessions
./manage.py clearsessions

echo "======== END `date` ========"
