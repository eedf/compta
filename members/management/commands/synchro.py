from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.management.base import BaseCommand, CommandError
from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import ForeignKey, OneToOneField
from django.utils.http import urlencode
from datetime import date as Date, timedelta as Timedelta
import json
from mptt.models import MPTTModel
import pika
import re
from sentry_sdk import capture_exception
import ssl
import requests
import traceback
from uuid import UUID
from ... import models


def parse_range(begin, end):
    begin = Date.fromisoformat(begin)
    if end is not None:
        end = Date.fromisoformat(end) + Timedelta(days=1)
    return DateRange(begin, end)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--skip', action='append', default=[])

    def create_permissions(self):
        self.stdout.write("Create permissions")
        for permission in Permission.objects.filter(
            content_type__app_label='accounting'
        ).exclude(name__startswith="Can "):
            data = {
                'name': permission.name,
                'app_label': permission.content_type.app_label,
                'model': permission.content_type.model,
                'codename': permission.codename,
                'model_verbose_name': permission.content_type.name,
            }
            response = requests.post(
                f'{settings.JEITO_HOST}/api/v4/permission/',
                headers={'Authorization': f'token {settings.JEITO_TOKEN}'},
                data=data,
            )
            assert response.status_code == 201, f"{response.status_code=}"

    def get_attributes(self, Model, remote_object, local_object=None):
        need_update = not local_object
        attributes = {}
        for field in Model.sync_fields:
            is_fk = isinstance(Model._meta.get_field(field), (ForeignKey, OneToOneField))
            # Get new value
            if hasattr(Model, f'to_{field}'):
                new_value = getattr(Model, f'to_{field}')(remote_object)
            elif field == 'dates':
                new_value = parse_range(remote_object['begin'], remote_object['end'])
            elif is_fk:
                new_value = remote_object[f'{field}_uuid']
            else:
                new_value = remote_object[field]
            # Set attribute
            if is_fk:
                attributes[f'{field}_id'] = new_value
            else:
                attributes[field] = new_value
            # Check if need update
            if not local_object:
                continue
            if is_fk:
                local_value = getattr(local_object, f'{field}_id')
            else:
                local_value = getattr(local_object, field)
            if isinstance(local_value, UUID):
                local_value = str(local_value)
            if local_value != new_value:
                need_update = True
        return attributes if need_update else None

    def sync_model(self, model_name, filters=None):
        self.stdout.write(f"{model_name}s: ", ending="")
        self.stdout.flush()
        Model = getattr(models, model_name)
        snake_model_name = re.sub('(?!^)([A-Z]+)', r'_\1', model_name).lower()
        next_url = f'{settings.JEITO_HOST}/api/v4/{snake_model_name}/?limit=1000&offset=0'
        if filters:
            next_url += '&' + urlencode(filters)
        uuids = []
        nb = 0
        nb_created = 0
        nb_updated = 0
        while next_url:
            response = requests.get(
                next_url,
                headers={'Authorization': f'token {settings.JEITO_TOKEN}'},
            )
            if not response.status_code == 200:
                raise CommandError(f"GET {next_url} returned status {response.status_code}")
            data = response.json()
            uuids.extend((remote_object['uuid'] for remote_object in data['results']))
            nb += len(data['results'])
            self.stdout.write(".", ending="")
            self.stdout.flush
            next_url = data['next']
            objects = {
                str(object.uuid): object
                for object in Model.objects.filter(uuid__in=(object['uuid'] for object in data['results']))
            }
            to_create = set()
            to_update = set()
            for remote_object in data['results']:
                local_object = objects.get(remote_object['uuid'])
                attributes = self.get_attributes(Model, remote_object, local_object)
                if not attributes:
                    continue
                attributes['uuid'] = remote_object['uuid']
                if issubclass(Model, MPTTModel):
                    attributes.update({'tree_id': 0, 'lft': 0, 'rght': 0, 'level': 0})
                object = Model(**attributes)
                if local_object:
                    to_update.add(object)
                else:
                    to_create.add(object)
            Model.objects.bulk_create(to_create)
            Model.objects.bulk_update(to_update, fields=Model.sync_fields)
            nb_created += len(to_create)
            nb_updated += len(to_update)
        to_delete = Model.objects.exclude(uuid__in=uuids)
        # Prevent integrity error for self referencing FK
        for field in Model._meta.get_fields():
            if isinstance(field, (ForeignKey, OneToOneField)) and field.related_model == Model:
                to_delete.update(**{field.name: None})
        nb_deleted = to_delete.count()
        self.to_delete.append(to_delete)
        self.stdout.write(f" read {nb}, create {nb_created}, update {nb_updated}, delete {nb_deleted}")
        if issubclass(Model, MPTTModel):
            Model.objects.rebuild()

    def complete_sync(self, skip):
        self.stdout.write("Complete sync…")
        self.create_permissions()
        self.to_delete = []
        for model in ('StructureType', 'TeamType', 'RoleConfig'):
            if model not in skip:
                self.sync_model(model)
        if 'RoleConfigPermission' not in skip:
            self.sync_model('RoleConfigPermission', {'app_label': 'accounting'})
        for model in ('Attribution', 'Structure', 'Team', 'Person', 'Role', 'Adherent', 'Employee',
                      'Adhesion', 'Employment', 'Function'):
            if model not in skip:
                self.sync_model(model)
        for to_delete in reversed(self.to_delete):
            to_delete.delete()

    def realtime_sync(self):
        self.stdout.write("Realtime sync…")
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=settings.BROKER_HOST,
                port=settings.BROKER_PORT,
                ssl_options=pika.SSLOptions(ssl.create_default_context()) if settings.BROKER_TLS else None,
                virtual_host=settings.BROKER_VIRTUAL_HOST,
                credentials=pika.credentials.PlainCredentials(
                    username=settings.BROKER_USERNAME,
                    password=settings.BROKER_PASSWORD,
                ),
            ),
        )
        channel = connection.channel()
        channel.exchange_declare(exchange='synchro_v4', exchange_type='topic')
        channel.queue_declare(queue='compta', exclusive=True)
        channel.queue_bind(exchange='synchro_v4', queue='compta', routing_key='jeito.*')
        self.stdout.write("Connected")

        MODELS = ('StructureType', 'Structure', 'TeamType', 'Team', 'Person', 'RoleConfig', 'Attribution', 'Role',
                  'RoleConfigPermission', 'Adherent', 'Adhesion', 'Employee', 'Employment', 'Function')
        lowercase_models = {model_name.lower(): getattr(models, model_name) for model_name in MODELS}

        def sync_object(method, body):
            data = json.loads(body)
            model_name = method.routing_key.split('.')[1]
            Model = lowercase_models.get(model_name)
            if not Model:
                return
            if data['operation'] == 'CREATE_UPDATE':
                attributes = self.get_attributes(Model, data['attributes'])
                self.stdout.write(f"Update or create {Model.__name__} {data['uuid']}")
                Model.objects.update_or_create(uuid=data['uuid'], defaults=attributes)
            elif data['operation'] == 'DELETE':
                self.stdout.write(f"Delete {Model.__name__} {data['uuid']}")
                Model.objects.get(uuid=data['uuid']).delete()

        def callback(ch, method, properties, body):
            try:
                sync_object(method, body)
            except Exception as e:
                self.stdout.write(self.style.ERROR(traceback.format_exc()))
                capture_exception(e)

        channel.basic_consume(queue='compta', on_message_callback=callback, auto_ack=True)
        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            connection.close()

    def handle(self, *args, **options):
        self.verbosity = int(options['verbosity'])
        if 'all' not in options['skip']:
            self.complete_sync(options['skip'])
        self.realtime_sync()
        self.stdout.write(self.style.SUCCESS(" Quit"))
