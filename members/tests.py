from django.test import TestCase
from .models import Structure, User


class StructureAutocompleteTest(TestCase):
    def test_q(self):
        user = User.objects.create(username='test')
        self.client.force_login(user)
        Structure.objects.create(uuid='00000000-0000-0000-0000-000000000001', name="ABC")
        Structure.objects.create(uuid='00000000-0000-0000-0000-000000000002', name="DEF")
        response = self.client.get('/structure/autocomplete/', {'q': "E"})
        self.assertJSONEqual(response.content, {
            'pagination': {'more': False},
            'results': [{'id': '00000000-0000-0000-0000-000000000002', 'selected_text': 'DEF', 'text': 'DEF'}],
        })
