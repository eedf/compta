from .models import User


def MasqueradeMiddleware(get_response):
    def middleware(request):
        actual_user_pk = request.session.get('actual_user_pk')
        if actual_user_pk:
            try:
                request.actual_user = User.objects.get(pk=actual_user_pk)
            except User.DoesNotExist:
                request.actual_user = None
        else:
            request.actual_user = None
        return get_response(request)

    return middleware
