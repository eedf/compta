# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import unicodedata
from django.utils.timezone import now, make_naive


def today():
    return make_naive(now()).date()


def current_season():
    today_ = today()
    if today_.month < 9:
        return today_.year
    else:
        return today_.year + 1


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore').decode()
    return only_ascii
