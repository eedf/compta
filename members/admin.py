from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin

from . import models


@admin.register(models.User)
class UserAdmin(AuthUserAdmin):
    list_display = ("username", "person", "email", "is_staff")
    list_filter = ("is_superuser", "groups")
    search_fields = ("username", "person__first_name", "person__last_name", "=person__email")
    fieldsets = (
        (None, {"fields": ("username", "password", "person")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        ("Important dates", {"fields": ("last_login", )}),
    )
    autocomplete_fields = ('person', )


@admin.register(models.StructureType)
class StructureTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )


@admin.register(models.Structure)
class StructureAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent')
    search_fields = ('name', )
    autocomplete_fields = ('parent', )


@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'email')
    search_fields = ('=uuid', 'last_name', 'first_name', '=email')
    ordering = ('last_name', 'first_name')
    autocomplete_fields = ('legal_guardian1', 'legal_guardian2')


@admin.register(models.TeamType)
class TeamTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'management')
    list_filter = ('management', )
    search_fields = ('name', )


@admin.register(models.Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('structure', 'type')
    list_filter = ('type', )
    search_fields = ('structure__name', )
    autocomplete_fields = ('structure', )


@admin.register(models.Function)
class FunctionAdmin(admin.ModelAdmin):
    list_display = ('name', 'person', 'team', 'dates')
    search_fields = ('person__last_name', 'person__first_name', '=person__adherent__id')
    autocomplete_fields = ('person', 'team')


@admin.register(models.Adherent)
class AdherentAdmin(admin.ModelAdmin):
    list_display = ('id', 'person')
    search_fields = ('person__last_name', 'person__first_name', '=id', '=uuid')
    autocomplete_fields = ('person', )


@admin.register(models.Adhesion)
class AdhesionAdmin(admin.ModelAdmin):
    list_display = ('season', 'adherent', 'dates', 'structure', 'canceled')
    list_filter = ('season', 'canceled')
    search_fields = ('adherent__person__last_name', 'adherent__person__first_name', '=adherent__id', '=uuidd')
    autocomplete_fields = ('adherent', 'structure')


class AttributionInline(admin.TabularInline):
    model = models.Attribution


@admin.register(models.RoleConfig)
class RoleConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'for_self', 'for_team', 'for_structure', 'for_sub_structures', 'for_all',
                    'only_electeds', 'only_employees', 'with_guardians', 'with_payers')
    list_filter = ('for_self', 'for_team', 'for_structure', 'for_sub_structures', 'for_all',
                   'only_electeds', 'only_employees', 'with_guardians', 'with_payers')
    search_fields = ('name', )
    filter_horizontal = ('permissions', )
    inlines = (AttributionInline, )


@admin.register(models.Attribution)
class AttributionAdmin(admin.ModelAdmin):
    list_display = ('role_config', 'structure_type', 'team_type', 'function_type_uuid', 'function_category')
    list_filter = ('role_config', 'structure_type', 'team_type', 'function_type_uuid', 'function_category')
    search_fields = ('role_config__name', )


@admin.register(models.Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('config', 'team', 'holder', 'proxy')
    list_filter = ('config', )
    search_fields = ('holder__last_name', 'holder__first_name', 'proxy__last_name', 'proxy__first_name')
    autocomplete_fields = ('team', 'holder', 'proxy')


class RoleConfigInline(admin.TabularInline):
    model = models.RoleConfig.permissions.through


@admin.register(models.Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'codename', 'name')
    list_filter = ('content_type__app_label', )
    search_fields = ('codename', 'name')
    inlines = [RoleConfigInline]
