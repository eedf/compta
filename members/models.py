from copy import deepcopy
from functools import reduce
from itertools import chain
import re
import uuid
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin, Permission
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.postgres.fields import ArrayField, DateRangeField
from django.contrib.postgres.search import TrigramSimilarity, TrigramStrictWordSimilarity
from django.db import models
from django.db.models import Q, Value
from django.db.models.functions import Concat, Greatest
from django.utils.timezone import now
from mptt.models import MPTTModel, TreeForeignKey
from members.utils import remove_accents


def current_season():
    today_ = now().date()
    if today_.month < 9:
        return today_.year
    else:
        return today_.year + 1


def q_or(a, b):
    """Or between Q objects. If on of the two is empty, the result is empty."""
    if a and b:
        return a | b
    return Q()


def q_add_prefix(q, prefix):
    if isinstance(q, Q):
        q.children = [q_add_prefix(child, prefix) for child in q.children]
        return q
    else:
        key, val = q
        return (prefix + key, val)


class PermissionQuerySetMixin:
    with_guardians_prefixes = ()
    with_payers_prefixes = ()

    def for_perm(self, perm, user, only_active=True):
        if user.is_superuser:
            return self
        if user.is_anonymous or not user.person:
            return self.none()
        return self.for_person_perm(perm, user.person, only_active)

    def for_person_perm(self, perm, person, only_active=True):
        q_list = []
        for config, team in person.perms(only_active).get(perm, ()):
            q = Q()
            if config.for_self:
                q |= self._q_for_self(person)
                for child in Person.objects.filter(Q(legal_guardian1=person) | Q(legal_guardian2=person)):
                    q |= self._q_for_self(child)
            if config.for_team:
                q |= self._q_for_team(team)
            if config.for_structure:
                q |= self._q_for_structure(team.structure)
            if config.for_sub_structures:
                q |= self._q_for_sub_structures(team.structure)
            if config.for_all:
                q = Q()
            elif not q:  # no for_* is set
                continue
            with_q = Q()
            if q and config.with_guardians:
                for prefix in self.with_guardians_prefixes:
                    with_q |= q_add_prefix(deepcopy(q), prefix)
            if q and config.with_payers:
                for prefix in self.with_payers_prefixes:
                    with_q |= q_add_prefix(deepcopy(q), prefix)
            q |= with_q
            if config.only_electeds:
                q &= self._q_only_electeds()
            if config.only_employees:
                q &= self._q_only_employees()
            q_list.append(q)
        if not q_list:
            return self.none()
        q = reduce(q_or, q_list)
        return self.filter(q).distinct()


class UserManager(BaseUserManager):
    def create_user(self, password=None, **kwargs):
        user = self.model(**kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, **kwargs):
        return self.create_user(is_superuser=True, **kwargs)


class User(PermissionsMixin, AbstractBaseUser):
    person = models.OneToOneField('Person', verbose_name="Personne", null=True, blank=True, on_delete=models.CASCADE)
    username = models.CharField("Nom d'utilisateur", max_length=150, null=True, unique=True,
                                help_text="Lettres (au moins une), chiffres et caractères ./+/-/_ seulement.",
                                validators=[UnicodeUsernameValidator()],
                                error_messages={"unique": "Un utilisateur avec ce nom existe déjà."})
    email = models.EmailField("Email", blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'

    class Meta:
        verbose_name = 'Utilisateur'

    def __str__(self):
        if self.person:
            return str(self.person)
        return self.username

    @property
    def is_staff(self):
        return self.is_superuser

    def has_perm(self, perm, obj=None, only_active=True):
        if super().has_perm(perm, obj):
            return True
        if not self.person:
            return False
        if perm not in self.person.perms(only_active):
            return False
        if not obj:
            return True
        return obj.__class__.objects.for_perm(perm, self, only_active).filter(pk=obj.pk).exists()


class PersonQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def search(self, q):
        if q.isdigit():
            return self.filter(Q(adherent__id=q) | Q(employee__id=q)).annotate(similarity=Value(1))
        match = re.fullmatch(r'([a-zA-Z]{3})-([0-9]{6})', q)
        if match:
            return self.filter(
                Q(adherent__id=match.group(2)) | Q(employee__id=match.group(2))
            ).annotate(similarity=Value(1))
        if "@" in q:
            return self.filter(Q(email=q) | Q(employee__email=q)).annotate(similarity=Value(1))
        q = remove_accents(q)
        return self.annotate(
            full_name=Concat('last_name', Value(" "), 'first_name'),
            similarity=Greatest(
                TrigramSimilarity('last_name__unaccent', q),
                TrigramSimilarity('first_name__unaccent', q),
                TrigramSimilarity('full_name__unaccent', q),
            ),
            word_similarity=TrigramStrictWordSimilarity(q, 'full_name__unaccent'),
        ).filter(
            word_similarity__gte=0.4,
        )


class Person(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField("Prénom", max_length=100)
    last_name = models.CharField("Nom d'usage", max_length=100)
    email = models.EmailField("Email", db_collation="case_insensitive", blank=True)
    address1 = models.CharField("Adresse ligne 1", max_length=100, blank=True)
    address2 = models.CharField("Adresse ligne 2", max_length=100, blank=True)
    address3 = models.CharField("Adresse ligne 3", max_length=100, blank=True)
    postal_code = models.CharField("Code postal", max_length=100, blank=True)
    city = models.CharField("Ville", max_length=100, blank=True)
    country = models.CharField("Pays", max_length=100, blank=True)
    legal_guardian1 = models.ForeignKey('Person', blank=True, null=True, related_name='protected1',
                                        on_delete=models.PROTECT, verbose_name="Responsable légal 1")
    legal_guardian2 = models.ForeignKey('Person', blank=True, null=True, related_name='protected2',
                                        on_delete=models.PROTECT, verbose_name="Responsable légal 2")

    sync_fields = ('first_name', 'last_name', 'email', 'address1', 'address2', 'address3', 'postal_code',
                   'city', 'country', 'legal_guardian1', 'legal_guardian2')

    objects = models.Manager.from_queryset(PersonQuerySet)()

    class Meta:
        verbose_name = "Personne"
        permissions = [
            ('masquerade_person', "Passer pour une autre personne"),
        ]

    def __str__(self):
        return f"{self.last_name} {self.first_name}"

    @property
    def address(self):
        ligne = self.address1
        if self.address2:
            ligne += f"\n{self.address2}"
        if self.address3:
            ligne += f"\n{self.address3}"
        ligne += f"\n{self.postal_code} {self.city}"
        if self.country and self.country.upper() != "FRANCE":
            ligne += f"\n{self.country}"
        return ligne

    @property
    def is_adherent(self):
        return Adhesion.objects.filter(
            adherent__person=self,
            canceled=False,
            season=current_season(),
        ).exists()

    @property
    def is_employee(self):
        return Employment.objects.filter(
            employee__person=self,
            dates__contains=now().date(),
        ).exists()

    @property
    def holder_roles(self):
        """Return {(config, team, active): fixed_roles}"""
        if not hasattr(self, '_holder_roles_cache'):
            roles = {}
            for config in RoleConfig.objects.for_no_function():
                roles.setdefault((config, None, True), set())
            for function in Function.objects.filter(person=self).active().select_related(
                'team', 'team__type', 'team__structure', 'team__structure__type',
            ):
                if function.type_uuid == FunctionType.EMPLOYEE:
                    active = self.is_employee
                else:
                    active = self.is_adherent
                for config in RoleConfig.objects.for_function(function):
                    structure_related = config.for_team or config.for_structure or config.for_sub_structures
                    team = function.team if structure_related else None
                    roles.setdefault((config, team, active), set())
                    roles[(config, team, active)] |= set(self.hold.filter(config=config, team=function.team))
            self._holder_roles_cache = roles
        return self._holder_roles_cache

    @property
    def proxy_roles(self):
        """Return {(config, team, active): fixed_roles}"""
        if not hasattr(self, '_proxy_roles_cache'):
            active = self.is_adherent or self.is_employee
            roles = {}
            for role in self.proxied.all():
                if (
                    not role.holder
                    or (role.config, role.team, True) in role.holder.holder_roles
                    or (role.config, role.team, False) in role.holder.holder_roles
                ):
                    key = (role.config, role.team, active)
                    roles.setdefault(key, set())
                    roles[key].add(role)
            self._proxy_roles_cache = roles
        return self._proxy_roles_cache

    @property
    def roles(self):
        """Return {(config, team, active): fixed_roles}"""
        if not hasattr(self, '_roles_cache'):
            self._roles_cache = {}
            for key in chain(self.holder_roles.keys(), self.proxy_roles.keys()):
                self._roles_cache[key] = self.holder_roles.get(key, set()) | self.proxy_roles.get(key, set())
        return self._roles_cache

    def perms(self, only_active=True):
        if not hasattr(self, '_perms_cache'):
            self._perms_cache = [{}, {}]
            for config, team, active in self.roles:
                for permission in config.permissions.select_related('content_type'):
                    perm = f'{permission.content_type.app_label}.{permission.codename}'
                    self._perms_cache[False].setdefault(perm, set())
                    self._perms_cache[False][perm].add((config, team))
                    if active:
                        self._perms_cache[True].setdefault(perm, set())
                        self._perms_cache[True][perm].add((config, team))
        return self._perms_cache[only_active]


class Adherent(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id = models.IntegerField("Numéro", unique=True)
    person = models.OneToOneField(Person, verbose_name="Personne", on_delete=models.PROTECT)
    duplicates = ArrayField(models.IntegerField(), verbose_name='Numéros doublons', default=list)

    sync_fields = ('id', 'person', 'duplicates')

    class Meta:
        verbose_name = "Adhérent·e"

    @property
    def id_str(self):
        return f"ADH-{self.id:06d}"

    def __str__(self):
        return "{} {} ({})".format(self.person.last_name, self.person.first_name, self.id_str)


class StructureType(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("Nom", max_length=100, unique=True)

    sync_fields = ('name', )

    class Meta:
        verbose_name = "Type de structure"
        verbose_name_plural = "Types de structure"
        ordering = ('name', )

    def __str__(self):
        return self.name


class Structure(MPTTModel):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("Nom", max_length=100)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children',
                            db_index=True, on_delete=models.PROTECT)
    type = models.ForeignKey(StructureType, null=True, on_delete=models.PROTECT)

    sync_fields = ('name', 'parent', 'type')

    class Meta:
        verbose_name = "Structure"
        ordering = ['lft']

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name


class Adhesion(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    adherent = models.ForeignKey(Adherent, verbose_name="Adhérent", related_name='adhesions', on_delete=models.PROTECT)
    season = models.IntegerField("Saison")
    dates = DateRangeField("Dates")
    structure = models.ForeignKey(Structure, verbose_name="Structure",
                                  related_name='adherents', on_delete=models.PROTECT)
    canceled = models.BooleanField("Annulée", default=False)

    sync_fields = ('adherent', 'season', 'dates', 'structure', 'canceled')

    class Meta:
        verbose_name = "Adhésion"

    def __str__(self):
        return f"{self.season - 1}/{self.season} {self.adherent}"


class TeamType(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("Nom", max_length=100)
    management = models.BooleanField("Équipe de gestion", default=False)

    sync_fields = ('name', 'management')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Type d'équipe"
        verbose_name_plural = "Types d'équipe"
        ordering = ('name', )


class Team(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    structure = models.ForeignKey(Structure, verbose_name="Structure",
                                  related_name='teams', on_delete=models.PROTECT)
    type = models.ForeignKey(TeamType, verbose_name="Type d'équipe", blank=True, null=True,
                             on_delete=models.PROTECT)

    sync_fields = ('structure', 'type')

    class Meta:
        verbose_name = "Équipe"

    def __str__(self):
        return f"{self.structure} #{self.id}"


class FunctionType(models.TextChoices):
    PARTICIPANT = '70d04cd1-0ac0-58da-b009-ae22e541147b', "Participant·e"
    MEMBER = '3f3e616d-a152-5d89-b0cd-27e733fed82d', "Membre"
    RESPONSIBLE = 'a7cdd9fa-42f4-5e9a-a4a3-b7b45951197b', "Responsable"
    TREASURER = '1b53cf76-3ced-5b0e-928a-093202e6cf64', "Trésorier·ère"
    DELEGATE = '54c62aa9-4eb1-5337-9367-ff683b4e3c42', "Délégué·e à l'AG"
    EMPLOYEE = '8252bfe0-c78f-5b51-ba4d-7ce350d2b54b', "Salarié·e"


class FunctionQuerySet(models.QuerySet):
    def active(self):
        return self.filter(
            dates__contains=now().date(),
            # FIXME check adhesion
        )


class Function(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person = models.ForeignKey(Person, verbose_name="Personne", on_delete=models.PROTECT, related_name='functions')
    team = models.ForeignKey(Team, verbose_name="Équipe", on_delete=models.PROTECT)
    dates = DateRangeField(verbose_name="Dates")
    name = models.CharField("Nom", max_length=100, blank=True)
    type_uuid = models.UUIDField(verbose_name="Type")

    sync_fields = ('person', 'team', 'dates', 'name', 'type_uuid')

    objects = models.Manager.from_queryset(FunctionQuerySet)()

    class Meta:
        verbose_name = "Fonction"
        ordering = ('name', )

    def __str__(self):
        return self.name


class RoleConfigPermission(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    roleconfig = models.ForeignKey("RoleConfig", verbose_name="Configuration du rôle", on_delete=models.PROTECT)
    permission = models.ForeignKey(Permission, verbose_name="Permission", on_delete=models.PROTECT)

    sync_fields = ('roleconfig', 'permission')

    class Meta:
        verbose_name = "Permission pour la configuration du rôle"
        verbose_name_plural = "Permissions pour la configuration des rôles"

    @classmethod
    def to_permission(cls, data):
        return Permission.objects.get(
            content_type__app_label=data['app_label'],
            content_type__model=data['model'],
            codename=data['codename'],
        ).id

    def __str__(self):
        return f'{self.roleconfig}:{self.permission}'


class RoleConfigQuerySet(models.QuerySet):
    def for_function(self, function):
        return self.filter(
            Q(attributions__structure_type=None) |
            Q(attributions__structure_type=function.team.structure.type),
            Q(attributions__team_type=None) |
            Q(attributions__team_type=function.team.type),
            Q(attributions__function_type_uuid=None) |
            Q(attributions__function_type_uuid=function.type_uuid),
            attributions__isnull=False,
        )

    def for_no_function(self):
        return self.filter(
            attributions__structure_type=None,
            attributions__team_type=None,
            attributions__function_type_uuid=None,
            attributions__isnull=False,
        )


class RoleConfig(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("Nom", max_length=100)
    description = models.TextField("Description")
    for_self = models.BooleanField("Pour soi", default=False)
    for_team = models.BooleanField("Pour son équipe", default=False)
    for_structure = models.BooleanField("Pour sa structure", default=False)
    for_sub_structures = models.BooleanField("Pour ses sous-structures", default=False)
    for_all = models.BooleanField("Pour l'association", default=False)
    only_electeds = models.BooleanField("Seulement les élus", default=False)
    only_employees = models.BooleanField("Seulement les salariés", default=False)
    with_guardians = models.BooleanField("Avec les responsables légaux", default=False)
    with_payers = models.BooleanField("Avec les payeurs", default=False)
    permissions = models.ManyToManyField(Permission, verbose_name="Permissions", through=RoleConfigPermission)

    sync_fields = ('name', 'description', 'for_self', 'for_team', 'for_structure', 'for_sub_structures',
                   'for_all', 'only_electeds', 'only_employees', 'with_guardians', 'with_payers')

    objects = models.Manager.from_queryset(RoleConfigQuerySet)()

    class Meta:
        verbose_name = "Configuration du rôle"
        verbose_name_plural = "Configuration des rôles"
        ordering = ('name', )

    def __str__(self):
        return self.name


class Attribution(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    role_config = models.ForeignKey(RoleConfig, verbose_name="Configuration du rôle", on_delete=models.PROTECT,
                                    related_name='attributions')
    structure_type = models.ForeignKey(StructureType, verbose_name="Type de structure", null=True,
                                       blank=True, on_delete=models.PROTECT)
    team_type = models.ForeignKey(TeamType, verbose_name="Type d'équipe", null=True, blank=True,
                                  on_delete=models.PROTECT)
    function_type_uuid = models.UUIDField(verbose_name="Type de fonction", null=True, blank=True)
    function_category = models.IntegerField(verbose_name="Catégorie de fonction", null=True, blank=True)

    sync_fields = ('role_config', 'structure_type', 'team_type', 'function_type_uuid',
                   'function_category')

    class Meta:
        verbose_name = "Attribution"

    def __str__(self):
        return f"{self.role_config}:" \
            f"structure_type={self.structure_type or '*'}," \
            f"team_type={self.team_type or '*'}," \
            f"function_type_uuid={self.function_type_uuid or '*'}," \
            f"function_category={self.function_category or '*'}"


class Role(models.Model):
    """
    Delegation: holder is not None
    Manual role: holder is null
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    config = models.ForeignKey(RoleConfig, verbose_name="Configuration du rôle", related_name='roles',
                               on_delete=models.PROTECT)
    team = models.ForeignKey(Team, verbose_name="Équipe", related_name='roles', on_delete=models.PROTECT)
    holder = models.ForeignKey(Person, verbose_name="Mandant", help_text="Personne qui donne le rôle",
                               null=True, blank=True, related_name='hold', on_delete=models.PROTECT)
    proxy = models.ForeignKey(Person, verbose_name="Madataire", help_text="Personne qui reçoit le rôle",
                              related_name='proxied', on_delete=models.PROTECT)

    sync_fields = ('config', 'team', 'holder', 'proxy')

    class Meta:
        verbose_name = "Rôle"

    def __str__(self):
        return f"{self.config}:{self.team}:{self.holder}:{self.proxy}"


class Employee(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id = models.IntegerField("Numéro", unique=True)
    person = models.OneToOneField(Person, verbose_name="Personne", on_delete=models.PROTECT)

    sync_fields = ('id', 'person', )


class Employment(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    employee = models.ForeignKey(Employee, verbose_name="Personne", related_name='employments',
                                 on_delete=models.PROTECT)
    dates = DateRangeField(verbose_name="Dates")

    sync_fields = ('employee', 'dates')
