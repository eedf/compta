from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_migrate
from django.dispatch import receiver


@receiver(post_migrate)
def rename_permissions(sender, verbosity, **kwargs):
    for model in sender.get_models(sender):
        content_type = ContentType.objects.get_for_model(model)
        for codename, name in tuple(model._meta.permissions) + tuple(getattr(model, 'renamed_permissions', ())):
            try:
                permission = Permission.objects.get(content_type=content_type, codename=codename)
            except Permission.DoesNotExist:
                continue
            if permission.name != name:
                if verbosity >= 2:
                    print(f"rename « {permission.name} » to « {name} »")
                permission.name = name
                permission.save()
