from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView
from dal import autocomplete
from functools import reduce
from operator import and_
import re
from .forms import MasqueradeForm
from .models import Structure, Person, User


class StructureAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Structure.objects.all()
        has_function = self.forwarded.get('has_function')
        if has_function:
            qs = qs.filter(teams__function__person__adherent=self.request.adherent).distinct()
        if self.q:
            tokens = re.split('( |-)', self.q)
            qs = qs.filter(reduce(and_, (Q(name__unaccent__icontains=token) for token in tokens)))
        return qs.order_by('name')


class PersonAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Person.objects
        if self.q:
            qs = qs.search(self.q).order_by('-similarity')
        return qs.distinct()


class OIDCFailView(TemplateView):
    template_name = 'members/oidc_fail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['error'] = self.request.GET.get('error')
        return context


class UserMasqueradeView(PermissionRequiredMixin, FormView):
    permission_required = 'members.masquerade_person'
    template_name = 'members/user_masquerade.html'
    form_class = MasqueradeForm
    success_url = reverse_lazy('home')

    def get_initial(self):
        return {'person': self.request.user.person}

    def form_valid(self, form):
        person = form.cleaned_data['person']
        fake_user, _ = User.objects.get_or_create(
            person=person,
            defaults={'username': person.uuid},
        )
        actual_user = self.request.actual_user or self.request.user
        login(self.request, fake_user, backend='django.contrib.auth.backends.ModelBackend')
        self.request.session['actual_user_pk'] = actual_user.pk
        return super().form_valid(form)
