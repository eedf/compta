# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.contrib import admin
from django.db.models import Q
from .models import (Account, Analytic, Entry, Transaction, Letter,
                     ThirdParty, Purchase, Sale, Journal, Year, Withdrawal,
                     Income, Expenditure, Cashing, Report, ReportItem,
                     Budget, BudgetEntry,
                     BankOperation, BankAccount, BankOperationType)


class HasScanListFilter(admin.SimpleListFilter):
    title = "Justificatif"
    parameter_name = 'scan'

    def lookups(self, request, model_admin):
        return (
            ('0', "Absent"),
        )

    def queryset(self, request, queryset):
        if self.value() == '0':
            return queryset.filter(
                Q(transaction__account__number__startswith='6') |
                Q(transaction__account__number__startswith='7'),
                scan=''
            )


@admin.register(Year)
class YearAdmin(admin.ModelAdmin):
    list_display = ('title', 'start', 'end', 'opened')
    search_fields = ('title', )
    list_filter = ('opened', )


@admin.register(Journal)
class JournalAdmin(admin.ModelAdmin):
    list_display = ('number', 'title')
    search_fields = ('=number', 'title')


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('number', 'title', 'enable_in_reports')
    search_fields = ('^number', 'title')
    list_editable = ('enable_in_reports', )
    list_filter = ('enable_in_reports', )


@admin.register(ThirdParty)
class ThirdPartyAdmin(admin.ModelAdmin):
    list_display = ('number', 'title', 'type', 'account', 'sage', 'iban')
    search_fields = ('=number', 'title', '=sage', '=iban')
    list_filter = ('type', )
    ordering = ('number', )


@admin.register(Analytic)
class AnalyticAdmin(admin.ModelAdmin):
    list_display = ('number', 'title')
    search_fields = ('=number', 'title')


class TransactionInline(admin.TabularInline):
    model = Transaction


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'year', 'date', 'balanced', 'has_scan', 'exported')
    search_fields = ('title', 'transaction__account__title', 'transaction__analytic__title')
    date_hierarchy = 'date'
    list_filter = (HasScanListFilter, 'year', 'journal', 'exported', 'transaction__analytic')
    inlines = (TransactionInline, )
    save_as = True

    def has_scan(self, obj):
        return bool(obj.scan)
    has_scan.short_description = "Justificatif"
    has_scan.boolean = True
    has_scan.admin_order_field = 'scan'


@admin.register(Purchase)
class PurchaseAdmin(EntryAdmin):
    list_display = ('date', 'title', 'deadline', 'number', 'balanced', 'has_scan', 'exported')
    search_fields = ('title', '=number', 'transaction__account__title', 'transaction__analytic__title')
    date_hierarchy = 'date'
    inlines = (TransactionInline, )
    save_as = True


@admin.register(Sale)
class SaleAdmin(EntryAdmin):
    list_display = ('date', 'title', 'number', 'balanced', 'has_scan', 'exported')
    search_fields = ('title', '=number', 'transaction__account__title', 'transaction__analytic__title')
    date_hierarchy = 'date'
    inlines = (TransactionInline, )
    save_as = True


@admin.register(Cashing)
class CashingAdmin(EntryAdmin):
    pass


@admin.register(Withdrawal)
class WithdrawalAdmin(EntryAdmin):
    pass


@admin.register(Income)
class IncomeAdmin(EntryAdmin):
    pass


@admin.register(Expenditure)
class ExpenditureAdmin(EntryAdmin):
    pass


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    search_fields = ('title', '^account__number', 'account__title', '=expense', '=revenue')
    date_hierarchy = 'entry__date'
    list_display = ('date', 'account', 'analytic', 'title', 'expense', 'revenue')
    list_filter = ('analytic', 'account')


class ReportItemInline(admin.TabularInline):
    model = ReportItem


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('date', 'title', 'person')
    search_fields = ('title', )
    date_hierarchy = 'date'
    inlines = (ReportItemInline, )
    raw_id_fields = ('person', 'treasurer', 'purchase')
    save_as = True


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ('iban', 'title', 'account', 'journal', 'default')
    search_fields = ('iban', 'title', )


@admin.register(BankOperationType)
class BankOperationTypeAdmin(admin.ModelAdmin):
    list_display = ('title', )
    search_fields = ('title', )


@admin.register(BankOperation)
class BankOperationAdmin(admin.ModelAdmin):
    list_display = ('date', 'order', 'account', 'reference', 'type', 'amount')
    list_filter = ('account', 'type', 'year')
    search_fields = ('details', )
    date_hierarchy = 'date'
    raw_id_fields = ('transaction', )


@admin.register(Budget)
class BudgetAdmin(admin.ModelAdmin):
    list_display = ('title', )
    search_fields = ('title', )


@admin.register(BudgetEntry)
class BudgetEntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'account', 'analytic', 'amount')
    list_filter = ('budget', )
    search_fields = ('title', )


@admin.register(Letter)
class LetterAdmin(admin.ModelAdmin):
    inlines = (TransactionInline, )
