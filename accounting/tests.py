# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from freezegun import freeze_time
from django.test import TestCase, override_settings
import unittest
from .factories import (AccountFactory, AnalyticFactory, BankAccountFactory, PurchaseFactory, ThirdPartyFactory,
                        YearFactory)
from members.models import User
from .models import Purchase, Transaction


@override_settings(HOSTS={'accounting': 'testserver'})
class LoggedOutViewsTests(TestCase):
    def assertForbid(self, url, method='get'):
        response = getattr(self.client, method)(url)
        self.assertRedirects(response, f'/oidc/authenticate?fail=/oidc/fail/&next={url}', fetch_redirect_response=False)

    def test_purchase_list(self):
        year = YearFactory.create()
        self.assertForbid('/{}/purchase/'.format(year.pk))

    def test_purchase_detail(self):
        purchase = PurchaseFactory.create()
        self.assertForbid(f'/{purchase.year.pk}/purchase/{purchase.pk}/')

    def test_purchase_update_get(self):
        purchase = PurchaseFactory.create()
        self.assertForbid(f'/{purchase.year.pk}/purchase/{purchase.pk}/update/')

    def test_purchase_update_post(self):
        purchase = PurchaseFactory.create()
        self.assertForbid(f'/{purchase.year.pk}/purchase/{purchase.pk}/update/', method='post')

    def test_purchase_create_get(self):
        year = YearFactory.create()
        self.assertForbid('/{}/purchase/create/'.format(year.pk))

    def test_purchase_create_post(self):
        year = YearFactory.create()
        self.assertForbid('/{}/purchase/create/'.format(year.pk), method='post')


@unittest.skip
@freeze_time("2015-01-01")
@override_settings(HOSTS={'accounting': 'testserver'})
class LoggedInViewsTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_superuser()

    def setUp(self):
        self.client.force_login(user=self.user)

    def test_purchase_list(self):
        year = YearFactory.create(title=2000)
        PurchaseFactory.create(year=year, transactions__amount=1.11)
        PurchaseFactory.create(year=year, transactions__amount=2.22)
        response = self.client.get('/{}/purchase/'.format(year.pk))
        self.assertContains(response, "Factures fournisseur 2000")
        self.assertContains(response, "1,11")
        self.assertContains(response, "2,22")
        self.assertContains(response, "3,33")

    def test_purchase_detail(self):
        purchase = PurchaseFactory.create(transactions__amount=1.11)
        response = self.client.get('/{}/purchase/{}/'.format(purchase.year.pk, purchase.pk))
        self.assertContains(response, "Test purchase")
        self.assertContains(response, "1,11", count=4)

    def test_purchase_update_get(self):
        purchase = PurchaseFactory.create(transactions__amount=1.11)
        response = self.client.get('/{}/purchase/{}/update/'.format(purchase.year.pk, purchase.pk))
        self.assertContains(response, "Modifier l'achat Test purchase")
        self.assertContains(response, 'name="amount" value="1.11"')
        self.assertContains(response, 'name="transaction_set-0-amount" value="1.11"')

    def test_purchase_update_post(self):
        purchase = PurchaseFactory.create()
        data = {
            'title': "Intitulé modifié",
            'date': '2010-06-18',
            'thirdparty': ThirdPartyFactory.create(type=1).pk,
            'amount': '1.42',
            'deposit': 0,
            'account': BankAccountFactory.create().pk,
            'transaction_set-TOTAL_FORMS': '1',
            'transaction_set-INITIAL_FORMS': '1',
            'transaction_set-0-account': AccountFactory.create(prefix=6000000).pk,
            'transaction_set-0-analytic': AnalyticFactory.create().pk,
            'transaction_set-0-amount': '1.42',
            'transaction_set-0-id': purchase.transaction_set.get(account__number__startswith='6').pk,
        }
        response = self.client.post('/{}/purchase/{}/update/'.format(purchase.year.pk, purchase.pk), data)
        self.assertRedirects(response, '/{}/purchase/'.format(purchase.year.pk))
        purchase.refresh_from_db()
        self.assertEqual(purchase.title, "Intitulé modifié")

    def test_purchase_update_form_invalid(self):
        purchase = PurchaseFactory.create()
        data = {
            'title': "Intitulé modifié",
            'date': '2010-06-18',
            'thirdparty': ThirdPartyFactory.create(type=1).pk,
            # missing 'amount'
            'transaction_set-TOTAL_FORMS': '1',
            'transaction_set-INITIAL_FORMS': '1',
            'transaction_set-0-account': AccountFactory.create(prefix=6000000).pk,
            'transaction_set-0-analytic': AnalyticFactory.create().pk,
            'transaction_set-0-amount': '1.42',
            'transaction_set-0-id': purchase.transaction_set.get(account__number__startswith='6').pk,
        }
        response = self.client.post('/{}/purchase/{}/update/'.format(purchase.year.pk, purchase.pk), data)
        self.assertContains(response, "Ce champ est obligatoire")

    def test_purchase_update_formset_invalid(self):
        purchase = PurchaseFactory.create()
        data = {
            'title': "Intitulé modifié",
            'date': '2010-06-18',
            'thirdparty': ThirdPartyFactory.create(type=1).pk,
            'amount': '1.42',
            'transaction_set-TOTAL_FORMS': '1',
            'transaction_set-INITIAL_FORMS': '1',
            'transaction_set-0-account': AccountFactory.create(prefix=6000000).pk,
            'transaction_set-0-analytic': AnalyticFactory.create().pk,
            # missing 'transaction_set-0-amount'
            'transaction_set-0-id': purchase.transaction_set.get(account__number__startswith='6').pk,
        }
        response = self.client.post('/{}/purchase/{}/update/'.format(purchase.year.pk, purchase.pk), data)
        self.assertContains(response, "Ce champ est obligatoire")

    def test_purchase_create_get(self):
        year = YearFactory.create()
        response = self.client.get('/{}/purchase/create/'.format(year.pk))
        self.assertContains(response, "Ajouter un achat")

    def test_purchase_create_post(self):
        year = YearFactory.create()
        data = {
            'title': "Nouvel intitulé",
            'date': '2010-06-18',
            'thirdparty': ThirdPartyFactory.create(type=1).pk,
            'amount': '1.42',
            'deposit': 0,
            'account': BankAccountFactory.create().pk,
            'transaction_set-TOTAL_FORMS': '1',
            'transaction_set-INITIAL_FORMS': '0',
            'transaction_set-0-account': AccountFactory.create(prefix=6000000).pk,
            'transaction_set-0-analytic': AnalyticFactory.create().pk,
            'transaction_set-0-amount': '1.42',
        }
        response = self.client.post('/{}/purchase/create/'.format(year.pk), data)
        self.assertRedirects(response, '/{}/purchase/'.format(year.pk))
        self.assertQuerysetEqual(Purchase.objects.all(), ["<Purchase: Nouvel intitulé>"])
        self.assertQuerysetEqual(
            Transaction.objects.all(),
            ["<Transaction: Nouvel intitulé>", "<Transaction: Nouvel intitulé>"],
            ordered=False
        )

    def test_purchase_create_form_invalid(self):
        year = YearFactory.create()
        data = {
            'title': "Nouvel intitulé",
            'date': '2010-06-18',
            'thirdparty': ThirdPartyFactory.create(type=1).pk,
            # missing 'amount'
            'transaction_set-TOTAL_FORMS': '1',
            'transaction_set-INITIAL_FORMS': '0',
            'transaction_set-0-account': AccountFactory.create(prefix=6000000).pk,
            'transaction_set-0-analytic': AnalyticFactory.create().pk,
        }
        response = self.client.post('/{}/purchase/create/'.format(year.pk), data)
        self.assertContains(response, "Ce champ est obligatoire")

    def test_purchase_create_formset_invalid(self):
        year = YearFactory.create()
        data = {
            'title': "Nouvel intitulé",
            'date': '2010-06-18',
            'thirdparty': ThirdPartyFactory.create(type=1).pk,
            'amount': '1.42',
            'deposit': 0,
            'transaction_set-TOTAL_FORMS': '1',
            'transaction_set-INITIAL_FORMS': '0',
            'transaction_set-0-account': AccountFactory.create(prefix=6000000).pk,
            'transaction_set-0-analytic': AnalyticFactory.create().pk,
            # missing 'transaction_set-0-amount'
        }
        response = self.client.post('/{}/purchase/create/'.format(year.pk), data)
        self.assertContains(response, "Ce champ est obligatoire")
