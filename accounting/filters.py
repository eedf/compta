# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from dal import autocomplete, forward
from functools import reduce
from operator import and_
from django import forms
from django.db.models import Sum, Q
import django_filters
from .models import Analytic, Account, ThirdParty, Transaction, BankOperation, BankAccount, BankOperationType, Budget


class BaseFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'id': 'filter'}
        self.helper.form_class = 'form-inline'
        self.helper.form_method = 'get'


class BalanceFilterForm(BaseFilterForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'balance',
        )


class BalanceFilter(django_filters.FilterSet):
    balance = django_filters.ChoiceFilter(label="Solde", choices=(('D', "Débiteur"), ('C', "Créditeur")),
                                          method='filter_balance')

    class Meta:
        model = Transaction
        fields = ('balance', )
        form = BalanceFilterForm

    def __init__(self, aggregate, data, *args, **kwargs):
        self.aggregate = aggregate
        super().__init__(data, *args, **kwargs)
        self.queryset = self.queryset.exclude(**{self.aggregate: None})
        self.queryset = self.queryset.values(
            *(self.aggregate + '__id', self.aggregate + '__number', self.aggregate + '__title')
        )
        self.queryset = self.queryset.annotate(
            revenues=Sum('revenue'), expenses=Sum('expense'), balance=Sum('revenue') - Sum('expense')
        )
        self.queryset = self.queryset.order_by(*(self.aggregate + '__number', ))

    def filter_balance(self, qs, name, value):
        if value == 'D':
            return qs.filter(balance__lt=0)
        if value == 'C':
            return qs.filter(balance__gt=0)
        return qs


class ThirdPartyFilterForm(BaseFilterForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'type',
            'account',
            'balance',
            'q',
        )


class ThirdPartyFilter(django_filters.FilterSet):
    BALANCE_CHOICES = (
        ('C', "Créditeur"),
        ('CX', "Créditeur (hors avances)"),
        ('D', "Débiteur"),
        ('DX', "Débiteur (hors avances)"),
        ('N', "Non nul"),
        ('NX', "Non nul (hors avances)"),
        ('Z', "Nul"),
        ('ZX', "Nul (hors avances)"),
    )
    balance = django_filters.ChoiceFilter(label="Solde", choices=BALANCE_CHOICES, method='filter_balance')
    q = django_filters.CharFilter(label="Chercher", method='filter_q')

    class Meta:
        model = ThirdParty
        fields = ('type', 'account', 'balance', 'q')
        form = ThirdPartyFilterForm

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters['account'].field.widget = autocomplete.ModelSelect2(
            url='account-autocomplete',
            forward=[forward.Const('4', 'startswith')]
        )

    def filter_balance(self, qs, name, value):
        if value == 'C':
            return qs.filter(balance__gt=0)
        if value == 'CX':
            return qs.filter(balancex__gt=0)
        if value == 'D':
            return qs.filter(balance__lt=0)
        if value == 'DX':
            return qs.filter(balancex__lt=0)
        if value == 'N':
            return qs.exclude(balance=0)
        if value == 'NX':
            return qs.exclude(balancex=0)
        if value == 'Z':
            return qs.filter(balance=0)
        if value == 'ZX':
            return qs.filter(balancex=0)
        return qs

    def filter_q(self, qs, name, value):
        def subfilter(word):
            return (
                Q(title__unaccent__icontains=word) |
                Q(number__startswith=word)
            )
        return qs.filter(reduce(and_, [subfilter(word) for word in value.split(" ")]))


class AccountFilterForm(BaseFilterForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'account',
            'thirdparty',
            'analytic',
            'lettered',
        )


class AccountFilter(django_filters.FilterSet):
    account = django_filters.ModelChoiceFilter(
        label="Compte", queryset=Account.objects,
        widget=autocomplete.ModelSelect2(url='account-autocomplete'),
    )
    thirdparty = django_filters.ModelChoiceFilter(
        label="Tiers", queryset=ThirdParty.objects,
        widget=autocomplete.ModelSelect2(url='thirdparty-autocomplete'),
    )
    analytic = django_filters.ModelChoiceFilter(
        label="Compte analytique", queryset=Analytic.objects,
        widget=autocomplete.ModelSelect2(url='analytic-autocomplete'),
    )
    lettered = django_filters.BooleanFilter(label="Lettré", method='filter_lettered')

    class Meta:
        model = Transaction
        fields = ('account', 'thirdparty', 'analytic', 'lettered')
        form = AccountFilterForm

    @property
    def qs(self):
        qs = super().qs.order_by('entry__date')
        qs = qs.select_related('entry', 'account', 'thirdparty', 'analytic')
        return qs

    def filter_lettered(self, qs, name, value):
        qs = qs.filter(Q(account__number__startswith='4') | Q(account__number__startswith='511'))
        qs = qs.exclude(letter__isnull=value)
        return qs


class BankOperationFilterForm(BaseFilterForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'account',
            'type',
            'reconcilied',
        )


class BankOperationFilter(django_filters.FilterSet):
    account = django_filters.ModelChoiceFilter(label="Compte", queryset=BankAccount.objects)
    type = django_filters.ModelChoiceFilter(label="Type", queryset=BankOperationType.objects)
    reconcilied = django_filters.BooleanFilter(label="Rapproché", method='filter_reconcilied')

    class Meta:
        model = BankOperation
        fields = ('account', 'type', 'reconcilied')
        form = BankOperationFilterForm

    @property
    def qs(self):
        qs = super().qs.order_by('date')
        qs = qs.select_related('account', 'type')
        return qs

    def filter_reconcilied(self, qs, name, value):
        qs = qs.exclude(transaction__isnull=value)
        return qs


class BankAccountFilterForm(BaseFilterForm):
    account = forms.ModelChoiceField(label="Compte", queryset=BankAccount.objects)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'account',
        )


class BudgetFilterForm(BaseFilterForm):
    budget = forms.ModelChoiceField(label="Budget", queryset=Budget.objects.all(), empty_label=None)
    analytic = forms.ModelChoiceField(label="Compte analytique", queryset=Analytic.objects.all(), required=False,
                                      widget=autocomplete.ModelSelect2(url='analytic-autocomplete'))
    comparison = forms.ChoiceField(label="Comparaison", choices=((1, "Prévi. / Réal."), (2, "Réal. / Prévi.")))

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('budget', Budget.objects.order_by('title').last())
        data.setdefault('comparison', 1)
        super().__init__(data, *args, **kwargs)
        self.helper.layout = Layout(
            'budget',
            'analytic',
            'comparison',
        )
