# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from accounting.models import Year


def get_accounting_year_pk(request):
    pk = request.session.get('accounting_year_pk')
    if pk:
        return pk
    try:
        return Year.objects.filter(opened=True).earliest('start').pk
    except Year.DoesNotExist:
        return 1


def accounting(request):
    return {
        'accounting_year_pk': get_accounting_year_pk(request),
    }
