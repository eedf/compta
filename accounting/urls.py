# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter
from . import views
from . import views_api_v1


router_v1 = DefaultRouter()
router_v1.register('year', views_api_v1.YearViewSet1)
router_v1.register('account', views_api_v1.AccountViewSet1)
router_v1.register('analytic', views_api_v1.AnalyticViewSet1)
router_v1.register('third_party', views_api_v1.ThirdPartyViewSet1)


def preprocessing_api_v1(endpoints):
    return [endpoint for endpoint in endpoints if endpoint[0].startswith("/api/v1/")]


urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('<int:year_pk>/entry/', views.EntryListView.as_view(), name='entry_list'),
    path('<int:year_pk>/entry/<int:pk>/', views.EntryView.as_view(), name='entry'),
    path('<int:year_pk>/entry.csv', views.EntryCsvView.as_view(), name='entry-csv'),
    path('<int:year_pk>/projection/', views.ProjectionView.as_view(), name='projection'),
    path('<int:year_pk>/balance/', views.BalanceView.as_view(), name='balance'),
    path('<int:year_pk>/account/', views.AccountView.as_view(), name='account'),
    path('<int:year_pk>/thirdparty/', views.ThirdPartyListView.as_view(), name='thirdparty_list'),
    path('<int:year_pk>/thirdparty/<int:pk>/', views.ThirdPartyDetailView.as_view(), name='thirdparty_detail'),
    path('<int:year_pk>/thirdparty/create/', views.ThirdPartyCreateView.as_view(), name='thirdparty_create'),
    path('<int:year_pk>/thirdparty/<int:pk>/update/', views.ThirdPartyUpdateView.as_view(),
         name='thirdparty_update'),
    path('<int:year_pk>/thirdparty/<int:pk>/delete/', views.ThirdPartyDeleteView.as_view(),
         name='thirdparty_delete'),
    path('<int:year_pk>/thirdparty.csv', views.ThirdPartyCsvView.as_view(), name='thirdparty-csv'),
    path('<int:year_pk>/budget/', views.BudgetView.as_view(), name='budget'),
    path('<int:year_pk>/budget/create/', views.BudgetCreateView.as_view(), name='budget_create'),
    path('<int:year_pk>/budgetentry/create/', views.BudgetEntryCreateView.as_view(), name='budgetentry-create'),
    path('<int:year_pk>/budgetentry/<int:pk>/update/', views.BudgetEntryUpdateView.as_view(),
         name='budgetentry_update'),
    path('<int:year_pk>/budgetentry/<int:pk>/delete/', views.BudgetEntryDeleteView.as_view(),
         name='budgetentry_delete'),
    path('<int:year_pk>/budget/<int:pk>/', views.BudgetAccountView.as_view(), name='budget-account'),
    path('<int:year_pk>/budget/<int:pk>/update/', views.BudgetUpdateView.as_view(), name='budget-update'),
    path('<int:year_pk>/budget/<int:pk>/delete/', views.BudgetDeleteView.as_view(), name='budget-delete'),
    path('<int:year_pk>/analytic-balance/', views.AnalyticBalanceView.as_view(), name='analytic-balance'),
    path('<int:year_pk>/analytic/create/', views.AnalyticCreateView.as_view(), name='analytic_create'),
    path('<int:year_pk>/analytic/<int:pk>/update/', views.AnalyticUpdateView.as_view(), name='analytic_update'),
    path('<int:year_pk>/analytic/<int:pk>/delete/', views.AnalyticDeleteView.as_view(), name='analytic_delete'),
    path('<int:year_pk>/cash-flow/', views.CashFlowView.as_view(), name='cash-flow'),
    path('<int:year_pk>/cash-flow/data/', views.CashFlowJsonView.as_view(), name='cash_flow_data'),
    path('<int:year_pk>/checks/', views.ChecksView.as_view(), name='checks'),
    path('<int:year_pk>/entry/<int:pk>/to_purchase/', views.EntryToPurchaseView.as_view(), name='entry_to_purchase'),
    path('<int:year_pk>/entry/<int:pk>/to_sale/', views.EntryToSaleView.as_view(), name='entry_to_sale'),
    path('<int:year_pk>/entry/<int:pk>/to_income/', views.EntryToIncomeView.as_view(), name='entry_to_income'),
    path('<int:year_pk>/entry/<int:pk>/to_expenditure/', views.EntryToExpenditureView.as_view(),
         name='entry_to_expenditure'),
    path('<int:year_pk>/purchase/', views.PurchaseListView.as_view(), name='purchase_list'),
    path('<int:year_pk>/purchase/<int:pk>/', views.PurchaseDetailView.as_view(), name='purchase_detail'),
    path('<int:year_pk>/purchase/create/', views.PurchaseCreateView.as_view(), name='purchase_create'),
    path('<int:year_pk>/purchase/<int:pk>/update/', views.PurchaseUpdateView.as_view(), name='purchase_update'),
    path('<int:year_pk>/purchase/<int:pk>/delete/', views.PurchaseDeleteView.as_view(), name='purchase_delete'),
    path('<int:year_pk>/sale/', views.SaleListView.as_view(), name='sale_list'),
    path('<int:year_pk>/sale/<int:pk>/', views.SaleDetailView.as_view(), name='sale_detail'),
    path('<int:year_pk>/sale/<int:pk>/invoice/', views.SaleInvoiceView.as_view(), name='sale_invoice'),
    path('<int:year_pk>/sale/create/', views.SaleCreateView.as_view(), name='sale_create'),
    path('<int:year_pk>/sale/<int:pk>/update/', views.SaleUpdateView.as_view(), name='sale_update'),
    path('<int:year_pk>/sale/<int:pk>/delete/', views.SaleDeleteView.as_view(), name='sale_delete'),
    path('<int:year_pk>/income/', views.IncomeListView.as_view(), name='income_list'),
    path('<int:year_pk>/income/<int:pk>/', views.IncomeDetailView.as_view(), name='income_detail'),
    path('<int:year_pk>/income/create/', views.IncomeCreateView.as_view(), name='income_create'),
    path('<int:year_pk>/income/<int:pk>/update/', views.IncomeUpdateView.as_view(), name='income_update'),
    path('<int:year_pk>/income/<int:pk>/delete/', views.IncomeDeleteView.as_view(), name='income_delete'),
    path('<int:year_pk>/expenditure/', views.ExpenditureListView.as_view(), name='expenditure_list'),
    path('<int:year_pk>/expenditure/<int:pk>/', views.ExpenditureDetailView.as_view(), name='expenditure_detail'),
    path('<int:year_pk>/expenditure/create/', views.ExpenditureCreateView.as_view(), name='expenditure_create'),
    path('<int:year_pk>/expenditure/<int:pk>/update/', views.ExpenditureUpdateView.as_view(),
         name='expenditure_update'),
    path('<int:year_pk>/expenditure/<int:pk>/delete/', views.ExpenditureDeleteView.as_view(),
         name='expenditure_delete'),
    path('<int:year_pk>/cashing/', views.CashingListView.as_view(), name='cashing_list'),
    path('<int:year_pk>/cashing/<int:pk>/', views.CashingDetailView.as_view(), name='cashing_detail'),
    path('<int:year_pk>/cashing/create/', views.CashingCreateView.as_view(), name='cashing_create'),
    path('<int:year_pk>/cashing/<int:pk>/update/', views.CashingUpdateView.as_view(), name='cashing_update'),
    path('<int:year_pk>/cashing/<int:pk>/delete/', views.CashingDeleteView.as_view(), name='cashing_delete'),
    path('<int:year_pk>/withdrawal/', views.WithdrawalListView.as_view(), name='withdrawal_list'),
    path('<int:year_pk>/withdrawal/<int:pk>/', views.WithdrawalDetailView.as_view(), name='withdrawal_detail'),
    path('<int:year_pk>/withdrawal/create/', views.WithdrawalCreateView.as_view(), name='withdrawal_create'),
    path('<int:year_pk>/withdrawal/<int:pk>/update/', views.WithdrawalUpdateView.as_view(),
         name='withdrawal_update'),
    path('<int:year_pk>/withdrawal/<int:pk>/delete/', views.WithdrawalDeleteView.as_view(),
         name='withdrawal_delete'),
    path('<int:year_pk>/depreciations/', views.DepreciationsUpdateView.as_view(), name='depreciations'),
    path('<int:year_pk>/year/', views.YearListView.as_view(), name='year_list'),
    path('<int:year_pk>/report/', views.ReportListView.as_view(), name='report_list'),
    path('<int:year_pk>/report/<int:pk>/', views.ReportDetailView.as_view(), name='report_detail'),
    path('<int:year_pk>/report/create/', views.ReportCreateView.as_view(), name='report_create'),
    path('<int:year_pk>/report/<int:pk>/update/', views.ReportUpdateView.as_view(), name='report_update'),
    path('<int:year_pk>/report/<int:pk>/delete/', views.ReportDeleteView.as_view(), name='report_delete'),
    path('<int:year_pk>/bankoperation/', views.BankOperationListView.as_view(), name='bankoperation_list'),
    path('<int:year_pk>/bankoperation/<int:pk>/', views.BankOperationDetailView.as_view(),
         name='bankoperation_detail'),
    path('<int:year_pk>/bankoperation/<int:operation_pk>/point/<int:transaction_pk>',
         views.BankOperationPointView.as_view(), name='bankoperation_point'),
    path('<int:year_pk>/bankoperation/<int:operation_pk>/unpoint/', views.BankOperationUnpointView.as_view(),
         name='bankoperation_unpoint'),
    path('bankoperation/upload/<str:account_title>/', views.BankOperationUploadView.as_view(),
         name='bankoperation_upload'),
    path('<int:year_pk>/letter/<int:pk>/', views.LetterDetailView.as_view(), name='letter_detail'),
    path('<int:year_pk>/letter/<int:pk>/delete/', views.LetterDeleteView.as_view(), name='letter_delete'),
    path('account-autocomplete/', views.AccountAutocomplete.as_view(), name='account-autocomplete'),
    path('analytic-autocomplete/', views.AnalyticAutocomplete.as_view(), name='analytic-autocomplete'),
    path('thirdparty-autocomplete/', views.ThirdPartyAutocomplete.as_view(), name='thirdparty-autocomplete'),
    path('api/v1/schema/', SpectacularAPIView.as_view(api_version='v1', custom_settings={
        'PREPROCESSING_HOOKS': ['accounting.urls.preprocessing_api_v1'],
    }), name='schema_v1'),
    path('api/v1/', SpectacularSwaggerView.as_view(url_name='schema_v1'), name='swagger_v1'),
    path('api/v1/', include(router_v1.urls)),
]
