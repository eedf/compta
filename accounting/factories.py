# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from factory import post_generation, LazyAttributeSequence, Sequence, SubFactory
from factory.django import DjangoModelFactory
from .models import Account, Analytic, BankAccount, Entry, Journal, Purchase, ThirdParty, Transaction, Year


class YearFactory(DjangoModelFactory):
    class Meta:
        model = Year

    title = Sequence(lambda n: "Test year {:03d}".format(n))
    start = '2010-05-13'
    end = '2011-05-12'
    opened = True


class AccountFactory(DjangoModelFactory):
    class Meta:
        model = Account

    class Params:
        prefix = 0

    number = LazyAttributeSequence(lambda o, n: "{:07d}".format(o.prefix + n))
    title = LazyAttributeSequence(lambda o, n: "Test account {:03d}".format(o.prefix + n))


class AnalyticFactory(DjangoModelFactory):
    class Meta:
        model = Analytic

    number = Sequence(lambda n: "{:03d}".format(n))
    title = Sequence(lambda n: "Test analytic {:03d}".format(n))


class ThirdPartyFactory(DjangoModelFactory):
    class Meta:
        model = ThirdParty

    number = Sequence(lambda n: "X{:03d}".format(n))
    title = Sequence(lambda n: "Test third party {:03d}".format(n))
    account = SubFactory(AccountFactory)
    type = 0


class JournalFactory(DjangoModelFactory):
    class Meta:
        model = Journal

    number = Sequence(lambda n: "J{:02d}".format(n))


class EntryFactory(DjangoModelFactory):
    class Meta:
        model = Entry

    title = Sequence(lambda n: "Test entry {:03d}".format(n))
    journal = SubFactory(JournalFactory)
    year = SubFactory(YearFactory)


class PurchaseFactory(EntryFactory):
    class Meta:
        model = Purchase

    title = Sequence(lambda n: "Test purchase {:03d}".format(n))
    journal = SubFactory(JournalFactory)

    @post_generation
    def transactions(self, create, extracted, amount=1234.56, **kwargs):
        self.transaction_set.add(TransactionFactory(
            entry=self,
            account__prefix=6000000,
            expense=amount
        ))
        self.transaction_set.add(TransactionFactory(
            entry=self,
            account__prefix=4010000,
            thirdparty=ThirdPartyFactory(),
            revenue=amount
        ))


class TransactionFactory(DjangoModelFactory):
    class Meta:
        model = Transaction

    entry = SubFactory(EntryFactory)
    account = SubFactory(AccountFactory)


class BankAccountFactory(DjangoModelFactory):
    class Meta:
        model = BankAccount

    title = 'Test bank account'
    iban = 'GB33BUKB20201555555555'
    account = SubFactory(AccountFactory)
    journal = SubFactory(JournalFactory)
