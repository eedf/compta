# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from csv import DictReader, DictWriter, QUOTE_NONNUMERIC
from codecs import iterdecode
from collections import OrderedDict
from dal import autocomplete
from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce
import json
from operator import and_, or_
from rest_framework.views import APIView
from rest_framework.permissions import DjangoModelPermissions
from weasyprint import HTML
from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.db.models import F, Q, Min, Max, Sum, Count, Value
from django.db.models.functions import Coalesce
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponseBadRequest
from django.urls import reverse, reverse_lazy
from django.utils.formats import date_format
from django.utils.timezone import now
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.views.generic import ListView, DetailView, TemplateView, View, CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin
from django_filters.views import FilterView
from .filters import (BalanceFilter, AccountFilter, BankAccountFilterForm, ThirdPartyFilter, BankOperationFilter,
                      BudgetFilterForm)
from .forms import (PurchaseForm, PurchaseFormSet, SaleForm, SaleFormSet, CashingForm, WithdrawalForm,
                    IncomeForm, ExpenditureForm, ExpenditureFormSet, ThirdPartyForm, AnalyticForm,
                    ReportForm, ReportFormSet, BudgetEntryForm, DepreciationForm, DepreciationFormSet)
from .models import (Account, BankAccount, Budget, Transaction, Entry, ThirdParty, Cashing, Withdrawal,
                     Letter, Purchase, Year, Sale, Income, Expenditure, Report, Analytic,
                     BankOperation, BankOperationType, BudgetEntry, Journal)
from members.utils import today


class ReadYearMixin():
    def dispatch(self, request, year_pk, *args, **kwargs):
        self.year = get_object_or_404(Year, pk=year_pk)
        request.session['accounting_year_pk'] = self.year.pk
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs['year'] = self.year
        kwargs['accounting_year_pk'] = self.year.pk
        return super().get_context_data(**kwargs)


class WriteYearMixin():
    def dispatch(self, request, year_pk, *args, **kwargs):
        self.year = get_object_or_404(Year, pk=year_pk)
        if not self.year.opened:
            return HttpResponseForbidden(f"L'exercice {self.year} est fermé. Vous ne pouvez pas le modifier.")
        request.session['accounting_year_pk'] = self.year.pk
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs['year'] = self.year
        kwargs['accounting_year_pk'] = self.year.pk
        return super().get_context_data(**kwargs)


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'accounting/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        years = Year.objects.filter(start__lte=today()).order_by('-start')
        if years:
            balance = Transaction.objects.filter(
                entry__year=years[0],
                account__number__regex=r'^[67]',
            ).aggregate(balance=Sum('revenue') - Sum('expense'))['balance'] or 0
            context['balance'] = balance
            cash = BankOperation.objects.filter(
                year=years[0],
            ).aggregate(cash=Sum('amount'))['cash'] or 0
            context['cash'] = cash
        if len(years) > 1:
            old_balance = Transaction.objects.filter(
                entry__year=years[1],
                entry__date__lte=today() - timedelta(days=365),
                account__number__regex=r'^[67]',
            ).aggregate(balance=Sum('revenue') - Sum('expense'))['balance'] or 0
            context['balance_diff'] = ((balance - old_balance) * 100 / abs(old_balance)) if old_balance else 0
            old_cash = BankOperation.objects.filter(
                year=years[1],
                date__lte=today() - timedelta(days=365),
            ).aggregate(cash=Sum('amount'))['cash'] or 0
            context['cash_diff'] = ((cash - old_cash) * 100 / abs(old_cash)) if old_cash else 0
        return context


class AccountAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = 'accounting.view_account'

    def get_queryset(self):
        qs = Account.objects.all()
        startswith = self.forwarded.get('startswith', '').split(',')
        if startswith:
            qs = qs.filter(reduce(or_, (Q(number__startswith=token) for token in startswith)))
        tokens = self.q.split()
        if tokens:
            qs = qs.filter(reduce(and_, (
                Q(title__unaccent__icontains=token) | Q(number__startswith=token)
                for token in tokens
            )))
        return qs


class AnalyticAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = 'accounting.view_analytic'

    def get_queryset(self):
        qs = Analytic.objects.all()
        if self.q:
            tokens = self.q.split()
            qs = qs.filter(reduce(and_, (
                Q(title__unaccent__icontains=token) | Q(number__istartswith=token)
                for token in tokens
            )))
        return qs


class ThirdPartyAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = 'accounting.view_thirdparty'

    def get_queryset(self):
        qs = ThirdParty.objects.all()
        if self.q:
            tokens = self.q.split()
            qs = qs.filter(reduce(and_, (
                Q(title__unaccent__icontains=token) | Q(number__istartswith=token)
                for token in tokens
            )))
        return qs


class ProjectionView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = "accounting/projection.html"
    permission_required = 'accounting.view_transaction'

    def get_queryset(self):
        qs = Transaction.objects.filter(entry__year=self.year)
        qs = qs.filter(account__number__regex=r'^[67]')
        qs = qs.values('account_id', 'account__number', 'account__title', 'analytic__id', 'analytic__title')
        qs = qs.order_by('account__number', 'analytic__title')
        qs = qs.annotate(solde=Sum(F('revenue') - F('expense')))
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.kwargs)  # year
        context['solde'] = sum([account['solde'] for account in self.object_list])
        return context


class AnalyticBalanceView(PermissionRequiredMixin, ReadYearMixin, FilterView):
    template_name = "accounting/analytic_balance.html"
    filterset_class = BalanceFilter
    permission_required = 'accounting.view_analytic'

    def get_queryset(self):
        return Transaction.objects.filter(entry__year=self.year)

    def get_filterset_kwargs(self, filterset_class):
        kwargs = super().get_filterset_kwargs(filterset_class)
        kwargs['aggregate'] = 'analytic'
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['data'] = self.object_list
        context['revenues'] = sum([analytic['revenues'] for analytic in self.object_list])
        context['expenses'] = sum([analytic['expenses'] for analytic in self.object_list])
        context['balance'] = sum([analytic['balance'] for analytic in self.object_list])
        return context


class AnalyticCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    form_class = AnalyticForm
    model = Analytic
    permission_required = 'accounting.add_analytic'

    def get_success_url(self):
        return reverse_lazy('analytic-balance', args=[self.year.pk])


class AnalyticUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    form_class = AnalyticForm
    model = Analytic
    permission_required = 'accounting.change_analytic'

    def get_success_url(self):
        return reverse_lazy('analytic-balance', args=[self.year.pk])


class AnalyticDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Analytic
    permission_required = 'accounting.delete_analytic'

    def get_success_url(self):
        return reverse_lazy('analytic-balance', args=[self.year.pk])


class ThirdPartyListView(PermissionRequiredMixin, ReadYearMixin, FilterView):
    template_name = "accounting/thirdparty_list.html"
    filterset_class = ThirdPartyFilter
    permission_required = 'accounting.view_thirdparty'

    def get_queryset(self):
        year_q = Q(transaction__entry__year=self.year)
        year_qx = year_q & ~Q(transaction__account__number__in=('4090000', '4190000'))
        qs = ThirdParty.objects.filter(transaction__entry__year=self.year).order_by('number')
        qs = qs.annotate(
            revenue=Coalesce(Sum('transaction__revenue', filter=year_q), Decimal(0)),
            expense=Coalesce(Sum('transaction__expense', filter=year_q), Decimal(0)),
            balance=Coalesce(
                Sum('transaction__revenue', filter=year_q) - Sum('transaction__expense', filter=year_q), Decimal(0)
            ),
            balancex=Coalesce(
                Sum('transaction__revenue', filter=year_qx) - Sum('transaction__expense', filter=year_qx), Decimal(0)
            ),
            not_lettered=Count('transaction', filter=Q(transaction__letter__isnull=True))
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['revenue'] = sum([thirdparty.revenue for thirdparty in self.object_list])
        context['expense'] = sum([thirdparty.expense for thirdparty in self.object_list])
        context['balance'] = sum([thirdparty.balance for thirdparty in self.object_list])
        return context


class LetterMixin:
    def post(self, request, *args, **kwargs):
        ids = [
            key[6:] for key, val in self.request.POST.items()
            if key.startswith('letter') and val == 'on'
        ]
        transactions = Transaction.objects.filter(id__in=ids)
        if transactions.filter(letter__isnull=False).exists():
            return HttpResponse("Certaines transactions sont déjà lettrées")
        if sum([transaction.balance for transaction in transactions]) != 0:
            return HttpResponse("Le lettrage n'est pas équilibré")
        if len(set([transaction.account_id for transaction in transactions])) > 1:
            return HttpResponse("Le lettrage doit concerner un seul compte général")
        if len(set([transaction.thirdparty_id for transaction in transactions])) > 1:
            return HttpResponse("Le lettrage doit concerner un seul tiers")
        if transactions:
            transactions.update(letter=Letter.objects.create())
        return HttpResponseRedirect(request.get_full_path())


class ThirdPartyDetailView(PermissionRequiredMixin, ReadYearMixin, LetterMixin, DetailView):
    context_object_name = 'thirdparty'
    model = ThirdParty
    permission_required = 'accounting.view_thirdparty'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        transactions = self.object.transaction_set.filter(entry__year=self.year).order_by('entry__date')
        balance = 0
        revenue = 0
        expense = 0
        for transaction in transactions:
            balance += transaction.revenue - transaction.expense
            transaction.accumulator = balance
            revenue += transaction.revenue
            expense += transaction.expense
        context['transactions'] = transactions
        context['revenue'] = revenue
        context['expense'] = expense
        context['balance'] = balance
        return context


class ThirdPartyCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    form_class = ThirdPartyForm
    model = ThirdParty
    permission_required = 'accounting.add_thirdparty'

    def get_success_url(self):
        return reverse_lazy('thirdparty_list', args=[self.year.pk])


class ThirdPartyUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    form_class = ThirdPartyForm
    model = ThirdParty
    permission_required = 'accounting.change_thirdparty'

    def get_success_url(self):
        return reverse_lazy('thirdparty_list', args=[self.year.pk])


class ThirdPartyDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = ThirdParty
    permission_required = 'accounting.delete_thirdparty'

    def get_success_url(self):
        return reverse_lazy('thirdparty_list', args=[self.year.pk])


class BalanceView(PermissionRequiredMixin, ReadYearMixin, FilterView):
    template_name = "accounting/balance.html"
    filterset_class = BalanceFilter
    permission_required = 'accounting.view_transaction'

    def get_queryset(self):
        return Transaction.objects.filter(entry__year=self.year)

    def get_filterset_kwargs(self, filterset_class):
        kwargs = super().get_filterset_kwargs(filterset_class)
        kwargs['aggregate'] = 'account'
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['data'] = self.object_list
        context['revenues'] = sum([account['revenues'] for account in self.object_list])
        context['expenses'] = sum([account['expenses'] for account in self.object_list])
        context['balance'] = sum([account['balance'] for account in self.object_list])
        return context


class AccountView(PermissionRequiredMixin, ReadYearMixin, LetterMixin, FilterView):
    template_name = "accounting/account.html"
    filterset_class = AccountFilter
    permission_required = 'accounting.view_transaction'

    def get_queryset(self):
        return Transaction.objects.filter(entry__year=self.year).order_by('entry__date', 'pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        solde = 0
        revenue = 0
        expense = 0
        for transaction in self.object_list:
            solde += transaction.revenue - transaction.expense
            transaction.solde = solde
            revenue += transaction.revenue
            expense += transaction.expense
        context['revenue'] = revenue
        context['expense'] = expense
        context['solde'] = solde
        return context


class EntryListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = "accounting/entry_list.html"
    model = Entry
    permission_required = 'accounting.view_entry'

    def get_queryset(self):
        return Entry.objects.filter(year=self.year).order_by('date', 'pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        revenue = 0
        expense = 0
        balance = 0
        for entry in self.object_list:
            revenue += entry.revenue
            expense += entry.expense
            balance += entry.balance
        context['revenue'] = revenue
        context['expense'] = expense
        context['balance'] = balance
        return context


class EntryView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    model = Entry
    permission_required = 'accounting.view_entry'

    def render_to_response(self, context, **response_kwargs):
        try:
            return HttpResponseRedirect(
                reverse('purchase_detail', args=[self.year.pk, self.object.purchase.pk])
            )
        except Purchase.DoesNotExist:
            pass
        try:
            return HttpResponseRedirect(
                reverse('sale_detail', args=[self.year.pk, self.object.sale.pk])
            )
        except Sale.DoesNotExist:
            pass
        try:
            return HttpResponseRedirect(
                reverse('income_detail', args=[self.year.pk, self.object.income.pk])
            )
        except Income.DoesNotExist:
            pass
        try:
            return HttpResponseRedirect(
                reverse('expenditure_detail', args=[self.year.pk, self.object.expenditure.pk])
            )
        except Expenditure.DoesNotExist:
            pass
        try:
            return HttpResponseRedirect(
                reverse('cashing_detail', args=[self.year.pk, self.object.cashing.pk])
            )
        except Cashing.DoesNotExist:
            pass
        try:
            return HttpResponseRedirect(
                reverse('withdrawal_detail', args=[self.year.pk, self.object.withdrawal.pk])
            )
        except Withdrawal.DoesNotExist:
            pass
        return super().render_to_response(context, **response_kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['transactions'] = self.object.transaction_set.order_by('account__number', 'analytic__title')
        return context


class CashFlowView(PermissionRequiredMixin, ReadYearMixin, TemplateView):
    template_name = 'accounting/cash_flow.html'
    permission_required = 'accounting.view_bankoperation'

    def get_context_data(self, **kwargs):
        account = self.request.GET.get('account')
        initial = self.request.GET.dict()
        initial.update({
            'account': account,
        })
        form = BankAccountFilterForm(initial=initial)
        context = super().get_context_data(**kwargs)
        context['form'] = form
        context.update(initial)
        return context


class CashFlowJsonView(PermissionRequiredMixin, ReadYearMixin, View):
    permission_required = 'accounting.view_bankoperation'

    def serie(self, year, account):
        self.today = (now() - timedelta(days=1)).date()
        start = year.start
        end = min(year.end, self.today)
        qs = BankOperation.objects.filter(year=year)
        if account:
            qs = qs.filter(account=account)
        qs = qs.order_by('-date').values('date').annotate(balance=Sum('amount'))
        qs = list(qs)
        data = OrderedDict()
        dates = [start + timedelta(days=n) for n in
                 range((end - start).days + 1)]
        balance = 0
        for d in dates:
            if qs and qs[-1]['date'] == d:
                balance += qs.pop()['balance']
            if d.month == 2 and d.day == 29:
                continue
            data[d] = balance
        return data

    def get(self, request):
        account_id = self.request.GET.get('account')
        if account_id:
            account = BankAccount.objects.get(id=account_id)
        else:
            account = None
        reference = Year.objects.filter(start__lt=self.year.start).last()
        data = self.serie(self.year, account)
        date_max = max(data.keys())
        date2 = date_max.strftime('%d/%m/%Y')
        nb2 = data[date_max]
        if reference:
            ref_data = self.serie(reference, account)
            ref_date_max = min(date_max + (reference.start - self.year.start), reference.end)
            date1 = ref_date_max.strftime('%d/%m/%Y')
            nb1 = ref_data[ref_date_max]
            diff = nb2 - nb1
            if nb1:
                percent = 100 * diff / nb1
            else:
                percent = 0
            comment = """Au <strong>{}</strong> : <strong>{:+0.2f}</strong> €<br>
                         Au <strong>{}</strong> : <strong>{:+0.2f}</strong> €,
                         c'est-à-dire <strong>{:+0.2f}</strong> €
                         (<strong>{:+0.1f} %</strong>)
                      """.format(date1, nb1, date2, nb2, diff, percent)
            data = {
                'labels': [date_format(x, 'b') if x.day == 1 else '' for x in ref_data.keys()],
                'series': [
                    list(ref_data.values()),
                    list(data.values()),
                ],
                'comment': comment,
            }
        else:
            comment = """Au <strong>{}</strong> : <strong>{:+0.2f}</strong> €
                      """.format(date2, nb2)
            data = {
                'labels': [date_format(x, 'b') if x.day == 1 else '' for x in data.keys()],
                'series': [
                    [],
                    list(data.values()),
                ],
                'comment': comment,
            }
        return JsonResponse(data)


class ThirdPartyCsvView(PermissionRequiredMixin, ReadYearMixin, ListView):
    model = ThirdParty
    fields = ('sage_number', 'title', 'type', 'account_number', 'iban', 'bic')
    permission_required = 'accounting.view_thirdparty'

    def render_to_response(self, context):
        response = HttpResponse(content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=tiers_{}_le_{}.txt'.format(
            self.year, now().strftime('%d_%m_%Y_a_%Hh%M')
        )
        writer = DictWriter(response, self.fields, delimiter=';', quoting=QUOTE_NONNUMERIC)
        writer.writeheader()
        for obj in self.object_list:
            writer.writerow({field: getattr(obj, field) for field in self.fields})
        return response


class EntryCsvView(PermissionRequiredMixin, ReadYearMixin, ListView):
    fields = (
        'journal_number', 'date_dmy', 'account_number', 'entry_id',
        'thirdparty_sage_number', '__str__', 'expense', 'revenue'
    )
    permission_required = 'accounting.view_transaction'

    def get_queryset(self):
        return Transaction.objects \
            .filter(entry__year=self.year, entry__exported=False) \
            .order_by('entry__id', 'id') \
            .select_related('entry', 'entry__journal', 'account', 'thirdparty')

    def render_to_response(self, context):
        response = HttpResponse(content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=ecritures_{}_le_{}.txt'.format(
            self.year, now().strftime('%d_%m_%Y_a_%Hh%M')
        )
        writer = DictWriter(response, self.fields, delimiter=';', quoting=QUOTE_NONNUMERIC)
        writer.writeheader()

        def get_value(obj, field):
            value = getattr(obj, field)
            if callable(value):
                value = value()
            return value

        for obj in self.object_list:
            writer.writerow({field: get_value(obj, field) for field in self.fields})
        return response


class ChecksView(PermissionRequiredMixin, ReadYearMixin, TemplateView):
    template_name = 'accounting/checks.html'
    permission_required = 'accounting.view_transaction'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        transactions = Transaction.objects.filter(entry__year=self.year)
        context['not_pointed'] = transactions.filter(account__number__startswith='512', operation=None)
        context['missing_analytic'] = transactions.filter(account__number__regex=r'^[67]', analytic__isnull=True)
        context['extra_analytic'] = transactions.filter(account__number__regex=r'^[^67]', analytic__isnull=False)
        context['missing_thirdparty'] = transactions.filter(account__number__regex=r'^[4]', thirdparty__isnull=True)
        context['extra_thirdparty'] = transactions.filter(account__number__regex=r'^[^4]', thirdparty__isnull=False)
        context['unbalanced_letters'] = Letter.objects.annotate(
            balance=Sum('transaction__revenue') - Sum('transaction__expense'),
            account_min=Min(Coalesce('transaction__account_id', 0)),
            account_max=Max(Coalesce('transaction__account_id', 0)),
            thirdparty_min=Min(Coalesce('transaction__thirdparty_id', 0)),
            thirdparty_max=Max(Coalesce('transaction__thirdparty_id', 0)),
        ).exclude(
            balance=0,
            account_min=F('account_max'),
            thirdparty_min=F('thirdparty_max')
        )
        context['unbalanced_reconciliations'] = BankOperation.objects.annotate(
            balance=F('transaction__expense') - F('transaction__revenue'),
        ).exclude(
            amount=F('balance'),
        )
        context['purchase_without_scan'] = Entry.objects.filter(year=self.year) \
            .filter(transaction__account__number__startswith='6') \
            .filter(scan='')
        context['pure_entries'] = Entry.objects.filter(year=self.year) \
            .filter(
                purchase__id=None, sale__id=None,
                income__id=None, expenditure__id=None,
                cashing__id=None, withdrawal__id=None
            )
        return context


class EntryToPurchaseView(PermissionRequiredMixin, WriteYearMixin, DetailView):
    model = Entry
    permission_required = 'accounting.change_entry'

    def get(self, request, *args, **kwargs):
        entry = self.get_object()
        purchase = Purchase(entry_ptr=entry)
        purchase.__dict__.update(entry.__dict__)
        purchase.save()
        return HttpResponseRedirect(reverse('purchase_detail', args=[self.year.pk, entry.pk]))


class EntryToSaleView(PermissionRequiredMixin, WriteYearMixin, DetailView):
    model = Entry
    permission_required = 'accounting.change_entry'

    def get(self, request, *args, **kwargs):
        entry = self.get_object()
        sale = Sale(entry_ptr=entry)
        sale.__dict__.update(entry.__dict__)
        sale.save()
        return HttpResponseRedirect(reverse('sale_detail', args=[self.year.pk, entry.pk]))


class EntryToIncomeView(PermissionRequiredMixin, WriteYearMixin, DetailView):
    model = Entry
    permission_required = 'accounting.change_entry'

    def get(self, request, *args, **kwargs):
        entry = self.get_object()
        income = Income(entry_ptr=entry)
        income.__dict__.update(entry.__dict__)
        income.save()
        return HttpResponseRedirect(reverse('income_detail', args=[self.year.pk, entry.pk]))


class EntryToExpenditureView(PermissionRequiredMixin, WriteYearMixin, DetailView):
    model = Entry
    permission_required = 'accounting.change_entry'

    def get(self, request, *args, **kwargs):
        entry = self.get_object()
        expenditure = Expenditure(entry_ptr=entry)
        expenditure.__dict__.update(entry.__dict__)
        expenditure.method = 5
        expenditure.save()
        return HttpResponseRedirect(reverse('expenditure_detail', args=[self.year.pk, entry.pk]))


class PurchaseListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/purchase_list.html'
    permission_required = 'accounting.view_purchase'

    def get_queryset(self):
        return Purchase.objects.filter(year=self.year).order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expense = 0
        for entry in self.object_list:
            expense += entry.expense
        context['expense'] = expense
        return context


class PurchaseDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/purchase_detail.html'
    context_object_name = 'purchase'
    permission_required = 'accounting.view_purchase'

    def get_queryset(self):
        return Purchase.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['amount'] = 0
        try:
            context['provider_transaction'] = self.object.transaction_set \
                .exclude(account__number='4090000') \
                .get(account__number__startswith='4')
        except Transaction.DoesNotExist:
            pass
        else:
            context['amount'] += context['provider_transaction'].amount_revenue
            context['thirdparty'] = context['provider_transaction'].thirdparty
        try:
            context['deposit_transaction'] = self.object.transaction_set.get(account__number='4090000')
        except Transaction.DoesNotExist:
            pass
        else:
            context['amount'] += context['deposit_transaction'].revenue
            context['thirdparty'] = context['deposit_transaction'].thirdparty
        expense_transactions = self.object.transaction_set.exclude(account__number__startswith='4') \
            .order_by('account__number', 'analytic__title')
        context['expense_transactions'] = expense_transactions
        return context


class PurchaseCreateView(PermissionRequiredMixin, WriteYearMixin, TemplateView):
    template_name = 'accounting/purchase_form.html'
    permission_required = 'accounting.add_purchase'

    def dispatch(self, request, *args, **kwargs):
        if 'report' in self.request.GET:
            self.report = get_object_or_404(Report, pk=self.request.GET['report'])
            assert self.report.signature_date
            assert not self.report.countersignature_date
            assert self.report.person != self.request.user
        else:
            self.report = None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        form_initial = {}
        formset_initial = []
        if self.report:
            form_initial.update({
                'title': "NdF {} {} {}".format(
                    self.report.person.first_name,
                    self.report.person.last_name,
                    self.report.title
                ),
                'date': self.report.signature_date,
                'amount': 0,
            })
            for item in self.report.reportitem_set.all():
                formset_initial.append({
                    'account': item.account,
                    'amount': item.expense,
                })
                form_initial['amount'] += item.expense
        if 'form' not in kwargs:
            kwargs['form'] = PurchaseForm(self.year, initial=form_initial)
        if 'formset' not in kwargs:
            kwargs['formset'] = PurchaseFormSet(initial=formset_initial)
        return kwargs

    def post(self, request, *args, **kwargs):
        form = PurchaseForm(self.year, data=self.request.POST, files=self.request.FILES)
        formset = PurchaseFormSet(instance=form.instance, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            purchase = form.save()
            purchase.save()
            formset.save()
            if self.report:
                self.report.countersignature_date = today()
                self.report.purchase = purchase
                self.report.save()
                if not purchase.scan:
                    purchase.scan = self.report.scan
                    purchase.save()
            if 'add-row' in self.request.POST:
                return HttpResponseRedirect(reverse_lazy('purchase_update', args=[self.year.pk, purchase.pk]))
            else:
                return HttpResponseRedirect(reverse_lazy('purchase_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class PurchaseUpdateView(PermissionRequiredMixin, WriteYearMixin, SingleObjectMixin, TemplateView):
    template_name = 'accounting/purchase_form.html'
    model = Purchase
    permission_required = 'accounting.change_purchase'

    def get_queryset(self):
        return Purchase.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = PurchaseForm(self.year, instance=self.object)
        if 'formset' not in kwargs:
            kwargs['formset'] = PurchaseFormSet(instance=self.object)
        return super().get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = PurchaseForm(self.year, instance=self.object, data=self.request.POST, files=self.request.FILES)
        formset = PurchaseFormSet(instance=self.object, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            purchase = form.save()
            formset.save()
            if 'add-row' in self.request.POST:
                return HttpResponseRedirect(reverse_lazy('purchase_update', args=[self.year.pk, purchase.pk]))
            else:
                return HttpResponseRedirect(reverse_lazy('purchase_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class PurchaseDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Purchase
    permission_required = 'accounting.delete_purchase'

    def get_success_url(self):
        return reverse_lazy('purchase_list', args=[self.year.pk])


class SaleListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/sale_list.html'
    permission_required = 'accounting.view_sale'

    def get_queryset(self):
        return Sale.objects.filter(year=self.year).order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        revenue = 0
        for entry in self.object_list:
            revenue += entry.revenue
        context['revenue'] = revenue
        return context


class SaleDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/sale_detail.html'
    context_object_name = 'sale'
    permission_required = 'accounting.view_sale'

    def get_queryset(self):
        return Sale.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['amount'] = 0
        try:
            context['client_transaction'] = self.object.transaction_set \
                .exclude(account__number='4190000') \
                .get(account__number__startswith='4')
        except Transaction.DoesNotExist:
            pass
        else:
            context['amount'] += context['client_transaction'].amount_expense
            context['thirdparty'] = context['client_transaction'].thirdparty
        try:
            context['deposit_transaction'] = self.object.transaction_set.get(account__number='4190000')
        except Transaction.DoesNotExist:
            pass
        else:
            context['amount'] += context['deposit_transaction'].expense
            context['thirdparty'] = context['deposit_transaction'].thirdparty
        profit_transactions = self.object.transaction_set.filter(account__number__startswith='7') \
            .order_by('account__number', 'analytic__title')
        context['profit_transactions'] = profit_transactions
        return context


class SaleInvoiceView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/invoice.html'
    context_object_name = 'sale'
    permission_required = 'accounting.view_sale'

    def get_queryset(self):
        return Sale.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def render_to_response(self, context, **response_kwargs):
        template = get_template(self.template_name)
        try:
            deposit_transaction = self.object.transaction_set.get(account__number='4190000')
        except Transaction.DoesNotExist:
            deposit = 0
        else:
            deposit = deposit_transaction.expense
        letters = Letter.objects.filter(transaction=self.object.client_transaction)
        payments = Transaction.objects.filter(letter__in=letters, revenue__gt=0)
        context = {
            'sale': self.object,
            'transactions': self.object.transaction_set.filter(account__number__startswith='7'),
            'deposit': deposit,
            'balance': self.object.client_transaction.expense,
            'payments': payments,
        }
        markup = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename=facture-{self.object.number}.pdf'
        HTML(string=markup, base_url=str(settings.BASE_DIR)).write_pdf(response)
        return response


class SaleCreateView(PermissionRequiredMixin, WriteYearMixin, TemplateView):
    template_name = 'accounting/sale_form.html'
    permission_required = 'accounting.add_sale'

    def get_context_data(self, **kwargs):
        initial = self.request.GET.get('initial')
        if initial:
            data = json.loads(initial)
            sale_initial = data['sale']
            transactions_initial = data['transactions']
        else:
            sale_initial = {}
            transactions_initial = {}
        numbers = Sale.objects.filter(number__regex=f'^{self.year.start.year}-\\d+$').values_list('number', flat=True)
        max_number = max((int(number[5:]) for number in numbers), default=0)
        sale_initial['number'] = f'{self.year.start.year}-{max_number+1:03d}'
        if 'form' not in kwargs:
            kwargs['form'] = SaleForm(self.year, initial=sale_initial)
        if 'formset' not in kwargs:
            kwargs['formset'] = SaleFormSet(initial=transactions_initial)
        return kwargs

    def post(self, request, *args, **kwargs):
        form = SaleForm(self.year, data=self.request.POST, files=self.request.FILES)
        formset = SaleFormSet(instance=form.instance, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            sale = form.save()
            formset.save()
            if 'add-row' in self.request.POST:
                return HttpResponseRedirect(reverse_lazy('sale_update', args=[self.year.pk, sale.pk]))
            else:
                return HttpResponseRedirect(reverse_lazy('sale_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class SaleUpdateView(PermissionRequiredMixin, WriteYearMixin, SingleObjectMixin, TemplateView):
    template_name = 'accounting/sale_form.html'
    model = Sale
    permission_required = 'accounting.change_sale'

    def get_queryset(self):
        return Sale.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = SaleForm(self.year, instance=self.object)
        if 'formset' not in kwargs:
            kwargs['formset'] = SaleFormSet(instance=self.object)
        return super().get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = SaleForm(self.year, instance=self.object, data=self.request.POST, files=self.request.FILES)
        formset = SaleFormSet(instance=self.object, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            sale = form.save()
            formset.save()
            if 'add-row' in self.request.POST:
                return HttpResponseRedirect(reverse_lazy('sale_update', args=[self.year.pk, sale.pk]))
            else:
                return HttpResponseRedirect(reverse_lazy('sale_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class SaleDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Sale
    permission_required = 'accounting.delete_sale'

    def get_success_url(self):
        return reverse_lazy('sale_list', args=[self.year.pk])


class IncomeListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/income_list.html'
    permission_required = 'accounting.view_income'

    def get_queryset(self):
        return Income.objects.filter(year=self.year).order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expense = 0
        for entry in self.object_list:
            expense += entry.expense
        context['expense'] = expense
        return context


class IncomeDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/income_detail.html'
    context_object_name = 'income'
    permission_required = 'accounting.view_income'

    def get_queryset(self):
        return Income.objects.filter(year=self.year)


class IncomeCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    template_name = 'accounting/income_form.html'
    form_class = IncomeForm
    permission_required = 'accounting.add_income'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.year
        return kwargs

    def get_initial(self):
        initial = {}
        if 'thirdparty' in self.request.GET:
            thirdparty = get_object_or_404(ThirdParty, pk=self.request.GET['thirdparty'])
            transactions = Transaction.objects.filter(thirdparty=thirdparty, letter=None, entry__year=self.year)
            initial.update({
                'title': f"Paiement {' + '.join([str(transaction) for transaction in transactions])}",
                'amount': -sum([transaction.balance for transaction in transactions]),
                'thirdparty': thirdparty,
                'method': '5120000',
            })
        if 'sale' in self.request.GET:
            sale = get_object_or_404(Sale, pk=self.request.GET['sale'])
            client_transaction = sale.client_transaction
            initial.update({
                'title': "Paiement {}".format(sale.title),
                'amount': client_transaction.expense,
                'thirdparty': client_transaction.thirdparty,
            })
        if 'operation' in self.request.GET:
            operation = get_object_or_404(BankOperation, pk=self.request.GET['operation'])
            initial.update({
                'date': operation.date,
                'amount': operation.amount,
                'method': '5120000',  # Virement
            })
        return initial

    def form_valid(self, form):
        response = super().form_valid(form)
        if 'thirdparty' in self.request.GET:
            thirdparty = get_object_or_404(ThirdParty, pk=self.request.GET['thirdparty'])
            transactions = Transaction.objects.filter(thirdparty=thirdparty, letter=None, entry__year=self.year)
            if sum([transaction.balance for transaction in transactions]) == 0:
                letter = Letter.objects.create()
                transactions.update(letter=letter)
        if 'sale' in self.request.GET:
            sale = get_object_or_404(Sale, pk=self.request.GET['sale'])
            sale_transaction = sale.client_transaction
            income_transaction = self.object.client_transaction
            if sale_transaction.balance == -income_transaction.balance:
                letter = Letter.objects.create()
                sale_transaction.letter = letter
                sale_transaction.save()
                income_transaction.letter = letter
                income_transaction.save()
        if 'operation' in self.request.GET:
            operation = get_object_or_404(BankOperation, pk=self.request.GET['operation'])
            operation.transaction = self.object.cash_transaction
            operation.save()
        return response

    def get_success_url(self):
        return reverse_lazy('income_list', args=[self.year.pk])


class IncomeUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    template_name = 'accounting/income_form.html'
    form_class = IncomeForm
    permission_required = 'accounting.change_income'

    def get_queryset(self):
        return Income.objects.filter(year=self.year)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.year
        return kwargs

    def get_success_url(self):
        return reverse_lazy('income_list', args=[self.year.pk])


class IncomeDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Income
    permission_required = 'accounting.delete_income'

    def get_success_url(self):
        return reverse_lazy('income_list', args=[self.year.pk])


class ExpenditureListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/expenditure_list.html'
    permission_required = 'accounting.view_expenditure'

    def get_queryset(self):
        return Expenditure.objects.filter(year=self.year).order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        revenue = 0
        for entry in self.object_list:
            revenue += entry.revenue
        context['revenue'] = revenue
        return context


class ExpenditureDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/expenditure_detail.html'
    context_object_name = 'expenditure'
    permission_required = 'accounting.view_expenditure'

    def get_queryset(self):
        return Expenditure.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        provider_transactions = self.object.provider_transactions.order_by('thirdparty__number')
        context['provider_transactions'] = provider_transactions
        return context


class ExpenditureCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    template_name = 'accounting/expenditure_form.html'
    form_class = ExpenditureForm
    permission_required = 'accounting.add_expenditure'

    def get_context_data(self, **kwargs):
        form_initial = {}
        formset_initial = {}
        if 'thirdparty' in self.request.GET:
            thirdparty = get_object_or_404(ThirdParty, pk=self.request.GET['thirdparty'])
            transactions = Transaction.objects.filter(thirdparty=thirdparty, letter=None, entry__year=self.year)
            form_initial.update({
                'title': f"Paiement {' + '.join([str(transaction) for transaction in transactions])}",
                'method': 5,
            })
            formset_initial.update({
                'expense': sum([transaction.balance for transaction in transactions]),
                'thirdparty': thirdparty,
            })
        if 'purchase' in self.request.GET:
            purchase = get_object_or_404(Purchase, pk=self.request.GET['purchase'])
            provider_transaction = purchase.provider_transaction
            form_initial.update({
                'title': "Paiement {}".format(purchase.title),
            })
            formset_initial.update({
                'expense': purchase.revenue,
                'thirdparty': provider_transaction.thirdparty,
            })
        if 'operation' in self.request.GET:
            operation = get_object_or_404(BankOperation, pk=self.request.GET['operation'])
            if operation.type.title == 'CARTE':
                method = '1'
            elif operation.type.title == 'PAIEMENT DE CHEQUE':
                method = '2'
            elif operation.type.title == 'PRELEVEMENT SEPA':
                method = '4'
            elif operation.type.title == 'VIREMENT SEPA EMIS':
                method = '5'
            else:
                method = None
            form_initial.update({
                'date': operation.date,
                'method': method,
            })
            formset_initial.update({
                'expense': -operation.amount,
            })
        if 'form' not in kwargs:
            kwargs['form'] = ExpenditureForm(self.year, initial=form_initial)
        if 'formset' not in kwargs:
            kwargs['formset'] = ExpenditureFormSet(initial=[formset_initial])
        return kwargs

    def post(self, request, *args, **kwargs):
        form = ExpenditureForm(self.year, data=self.request.POST, files=self.request.FILES)
        formset = ExpenditureFormSet(instance=form.instance, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            expenditure = form.save(formset)
            formset.save()
            if 'thirdparty' in self.request.GET:
                thirdparty = get_object_or_404(ThirdParty, pk=self.request.GET['thirdparty'])
                transactions = Transaction.objects.filter(thirdparty=thirdparty, letter=None, entry__year=self.year)
                if sum([transaction.balance for transaction in transactions]) == 0:
                    letter = Letter.objects.create()
                    transactions.update(letter=letter)
            if 'purchase' in self.request.GET:
                purchase = get_object_or_404(Purchase, pk=self.request.GET['purchase'])
                purchase_transaction = purchase.provider_transaction
                expenditure_transaction = expenditure.provider_transactions.first()
                if purchase_transaction.revenue == expenditure_transaction.expense:
                    letter = Letter.objects.create()
                    purchase_transaction.letter = letter
                    purchase_transaction.save()
                    expenditure_transaction.letter = letter
                    expenditure_transaction.save()
            if 'operation' in self.request.GET:
                operation = get_object_or_404(BankOperation, pk=self.request.GET['operation'])
                operation.transaction = expenditure.cash_transaction
                operation.save()
            return HttpResponseRedirect(reverse_lazy('expenditure_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class ExpenditureUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    template_name = 'accounting/expenditure_form.html'
    form_class = ExpenditureForm
    permission_required = 'accounting.change_expenditure'

    def get_queryset(self):
        return Expenditure.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = ExpenditureForm(self.year, instance=self.object)
        if 'formset' not in kwargs:
            kwargs['formset'] = ExpenditureFormSet(instance=self.object)
        return super().get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = ExpenditureForm(self.year, instance=self.object, data=self.request.POST, files=self.request.FILES)
        formset = ExpenditureFormSet(instance=self.object, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            form.save(formset)
            formset.save()
            return HttpResponseRedirect(reverse_lazy('expenditure_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class ExpenditureDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Expenditure
    permission_required = 'accounting.delete_expenditure'

    def get_success_url(self):
        return reverse_lazy('expenditure_list', args=[self.year.pk])


class CashingListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/cashing_list.html'
    permission_required = 'accounting.view_cashing'

    def get_queryset(self):
        return Cashing.objects.filter(year=self.year).order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expense = 0
        for entry in self.object_list:
            expense += entry.expense
        context['expense'] = expense
        return context


class CashingDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/cashing_detail.html'
    context_object_name = 'cashing'
    permission_required = 'accounting.view_cashing'

    def get_queryset(self):
        return Cashing.objects.filter(year=self.year)


class CashingCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    template_name = 'accounting/cashing_form.html'
    form_class = CashingForm
    permission_required = 'accounting.add_cashing'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.year
        return kwargs

    def get_success_url(self):
        return reverse_lazy('cashing_list', args=[self.year.pk])


class CashingUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    template_name = 'accounting/cashing_form.html'
    form_class = CashingForm
    permission_required = 'accounting.change_cashing'

    def get_queryset(self):
        return Cashing.objects.filter(year=self.year)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.year
        return kwargs

    def get_success_url(self):
        return reverse_lazy('cashing_list', args=[self.year.pk])


class CashingDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Cashing
    permission_required = 'accounting.delete_cashing'

    def get_success_url(self):
        return reverse_lazy('cashing_list', args=[self.year.pk])


class WithdrawalListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/withdrawal_list.html'
    permission_required = 'accounting.view_withdrawal'

    def get_queryset(self):
        return Withdrawal.objects.filter(year=self.year).order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expense = 0
        for entry in self.object_list:
            expense += entry.expense
        context['expense'] = expense
        return context


class WithdrawalDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/withdrawal_detail.html'
    context_object_name = 'withdrawal'
    permission_required = 'accounting.view_withdrawal'

    def get_queryset(self):
        return Withdrawal.objects.filter(year=self.year)


class WithdrawalCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    template_name = 'accounting/withdrawal_form.html'
    form_class = WithdrawalForm
    permission_required = 'accounting.add_withdrawal'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.year
        return kwargs

    def get_success_url(self):
        return reverse_lazy('withdrawal_list', args=[self.year.pk])


class WithdrawalUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    template_name = 'accounting/withdrawal_form.html'
    form_class = WithdrawalForm
    permission_required = 'accounting.change_withdrawal'

    def get_queryset(self):
        return Withdrawal.objects.filter(year=self.year)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['year'] = self.year
        return kwargs

    def get_success_url(self):
        return reverse_lazy('withdrawal_list', args=[self.year.pk])


class WithdrawalDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Withdrawal
    permission_required = 'accounting.delete_withdrawal'

    def get_success_url(self):
        return reverse_lazy('withdrawal_list', args=[self.year.pk])


class YearListView(PermissionRequiredMixin, ReadYearMixin, ListView):
    model = Year
    permission_required = 'accounting.view_year'


class ReportListView(LoginRequiredMixin, ReadYearMixin, ListView):
    template_name = 'accounting/report_list.html'

    def get_queryset(self):
        qs = Report.objects.filter(year=self.year).order_by('-date', '-pk')
        if not self.request.user.has_perm('accounting.view_report'):
            qs = qs.filter(person=self.request.user.person)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expense = 0
        for entry in self.object_list:
            expense += entry.expense
        context['expense'] = expense
        return context


class ReportDetailView(LoginRequiredMixin, ReadYearMixin, DetailView):
    template_name = 'accounting/report_detail.html'
    context_object_name = 'report'

    def get_queryset(self):
        qs = Report.objects.filter(year=self.year)
        if not self.request.user.has_perm('accounting.view_report'):
            qs = qs.filter(person=self.request.user.person)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        items = self.object.reportitem_set.all()
        context['items'] = items
        context['modifiable'] = \
            self.year.opened and \
            not self.object.countersignature_date and \
            (self.request.user.has_perm('accounting.change_report') or self.object.person == self.request.user.person)
        return context


class ReportCreateView(LoginRequiredMixin, ReadYearMixin, TemplateView):
    template_name = 'accounting/report_form.html'

    def get_context_data(self, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = ReportForm(user=self.request.user)
        if 'formset' not in kwargs:
            kwargs['formset'] = ReportFormSet()
        kwargs['person'] = self.request.user.person
        return kwargs

    def post(self, request, *args, **kwargs):
        form = ReportForm(user=self.request.user, data=self.request.POST, files=self.request.FILES)
        formset = ReportFormSet(instance=form.instance, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            report = form.save(commit=False)
            if not self.request.user.has_perm('accounting.add_report'):
                report.person = request.user.person
            report.year = self.year
            report.signature_date = today()
            report.save()
            formset.save()
            return HttpResponseRedirect(reverse_lazy('report_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class ReportUpdateView(LoginRequiredMixin, ReadYearMixin, SingleObjectMixin, TemplateView):
    template_name = 'accounting/report_form.html'
    model = Report

    def get_queryset(self):
        qs = Report.objects.filter(year=self.year, countersignature_date=None)
        if not self.request.user.has_perm('accounting.change_report'):
            qs = qs.filter(person=self.request.user.person)
        return qs

    def get_context_data(self, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = ReportForm(user=self.request.user, instance=self.object)
        if 'formset' not in kwargs:
            kwargs['formset'] = ReportFormSet(instance=self.object)
        kwargs['person'] = self.request.user.person
        return super().get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = ReportForm(user=self.request.user, instance=self.object,
                          data=self.request.POST, files=self.request.FILES)
        formset = ReportFormSet(instance=self.object, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            return HttpResponseRedirect(reverse_lazy('report_list', args=[self.year.pk]))
        else:
            return self.render_to_response(self.get_context_data(form=form, formset=formset))


class ReportDeleteView(LoginRequiredMixin, ReadYearMixin, DeleteView):
    model = Report

    def get_queryset(self):
        qs = Report.objects.filter(year=self.year, countersignature_date=None)
        if not self.request.user.has_perm('accounting.delete_report'):
            qs = qs.filter(person=self.request.user.person)
        return qs

    def get_success_url(self):
        return reverse_lazy('report_list', args=[self.year.pk])


class BankOperationListView(PermissionRequiredMixin, ReadYearMixin, FilterView):
    template_name = "accounting/bankoperation_list.html"
    filterset_class = BankOperationFilter
    permission_required = 'accounting.view_bankoperation'

    def get_queryset(self):
        return BankOperation.objects.filter(year=self.year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        balance = 0
        for operation in context['object_list']:
            balance += operation.amount
            operation.balance = balance
        context['amount'] = sum([operation.amount for operation in self.object_list])
        return context


class BankOperationDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    model = BankOperation
    permission_required = 'accounting.view_bankoperation'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['payments'] = Transaction.objects.filter(
            Q(expense=self.object.amount, revenue=0) | Q(revenue=-self.object.amount, expense=0),
            account__number__startswith='512',
            entry__year__lte=self.year,
            operation=None,
        )
        context['purchases'] = Purchase.objects.filter(
            transaction__revenue=-self.object.amount,
            transaction__account__number__startswith='4',
            transaction__letter=None,
            year=self.year,
        )
        context['sales'] = Sale.objects.filter(
            transaction__expense=self.object.amount,
            transaction__account__number__startswith='4',
            transaction__letter=None,
            year=self.year,
        )
        return context


class BankOperationPointView(PermissionRequiredMixin, WriteYearMixin, View):
    permission_required = 'accounting.change_bankoperation'

    def get(self, request, operation_pk, transaction_pk):
        operation = BankOperation.objects.get(pk=operation_pk)
        transaction = Transaction.objects.get(pk=transaction_pk)
        operation.transaction = transaction
        operation.save()
        return HttpResponseRedirect(reverse('bankoperation_list', args=[self.year.pk]))


class BankOperationUnpointView(PermissionRequiredMixin, WriteYearMixin, View):
    permission_required = 'accounting.change_bankoperation'

    def get(self, request, operation_pk):
        operation = BankOperation.objects.get(pk=operation_pk)
        operation.transaction = None
        operation.save()
        return HttpResponseRedirect(reverse('bankoperation_list', args=[self.year.pk]))


class LetterDetailView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    model = Letter
    permission_required = 'accounting.view_letter'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        transactions = self.object.transaction_set.filter(entry__year=self.year).order_by('entry__date')
        balance = 0
        revenue = 0
        expense = 0
        for transaction in transactions:
            balance += transaction.revenue - transaction.expense
            transaction.accumulator = balance
            revenue += transaction.revenue
            expense += transaction.expense
        context['transactions'] = transactions
        context['revenue'] = revenue
        context['expense'] = expense
        context['balance'] = balance
        return context


class LetterDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Letter
    permission_required = 'accounting.delete_letter'

    def get_success_url(self):
        return reverse_lazy('account', args=[self.year.pk])


class BankOperationUploadView(APIView):
    queryset = BankOperation.objects.all()
    permission_classes = [DjangoModelPermissions]

    def post(self, request, account_title):
        account = get_object_or_404(BankAccount, title=account_title)
        reader = DictReader(iterdecode(request.stream, 'utf-8-sig'), delimiter=";")
        done = []
        for row in reader:
            if not row["Référence de l'opération"].strip():
                continue
            order = done.count(row)
            done.append(row)
            date = datetime.strptime(row["Date de l'opération"], '%d/%m/%Y').date()
            try:
                year = Year.objects.get(start__lte=date, end__gte=date, opened=True)
            except Year.DoesNotExist:
                return HttpResponseBadRequest(f"Pas d'exercice ouvert au {date}.")
            BankOperation.objects.get_or_create(
                account=account,
                year=year,
                date=date,
                order=order,
                reference=row["Référence de l'opération"],
                type=BankOperationType.objects.get_or_create(title=row["Type de l'opération"].rstrip())[0],
                amount=row["Montant"].replace(',', '.'),
                details="\n".join(row[f"Détail {i}"].rstrip() for i in range(1, 7) if row[f"Détail {i}"])
            )
        return HttpResponse("OK")


class BudgetCreateView(PermissionRequiredMixin, ReadYearMixin, CreateView):
    permission_required = 'accounting.add_budget'
    model = Budget
    fields = ('title', )

    def get_success_url(self):
        return reverse('budget', args=[self.year.pk]) + f'?budget={self.object.pk}'


class BudgetView(PermissionRequiredMixin, ReadYearMixin, TemplateView):
    template_name = "accounting/budget.html"
    permission_required = 'accounting.view_budget'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = BudgetFilterForm(self.request.GET)
        context['form'] = form
        if not form.is_valid():
            return context
        if self.request.GET.get('analytic'):
            analytic = get_object_or_404(Analytic, pk=self.request.GET['analytic'])
            analytic_q = Q(analytic=analytic)
        else:
            analytic = None
            analytic_q = Q()
        budget = form.cleaned_data['budget']
        budget_balance = 0
        real_balance = 0
        for class_num, class_title, sign in (('6', 'expenses', -1), ('7', 'revenues', +1)):
            budget_total = 0
            real_total = 0
            class_items = []
            for subclass in Account.objects.filter(number__startswith=class_num, number__endswith='00000'):
                items = []
                budget_subtotal = 0
                real_subtotal = 0
                for account in Account.objects.filter(number__startswith=subclass.number[:2]).order_by('number'):
                    budget_amount = BudgetEntry.objects.filter(
                        analytic_q, budget=budget, account=account
                    ).aggregate(
                        amount=Sum('amount')
                    )['amount'] or 0
                    real_amount = (Transaction.objects.filter(
                        analytic_q, entry__year=self.year, account=account
                    ).aggregate(
                        amount=Sum(F('revenue') - F('expense'))
                    )['amount'] or 0) * sign
                    if budget_amount or real_amount:
                        if form.cleaned_data['comparison'] == '1':
                            items.append({
                                'account': account,
                                'left_amount': budget_amount,
                                'right_amount': real_amount,
                                'diff_amount': real_amount - budget_amount,
                            })
                        else:
                            items.append({
                                'account': account,
                                'left_amount': real_amount,
                                'right_amount': budget_amount,
                                'diff_amount': budget_amount - real_amount,
                            })
                        budget_subtotal += budget_amount
                        real_subtotal += real_amount
                if form.cleaned_data['comparison'] == '1':
                    class_items.append({
                        'number': subclass.number[:2],
                        'title': subclass.title,
                        'items': items,
                        'left_subtotal': budget_subtotal,
                        'right_subtotal': real_subtotal,
                        'diff_subtotal': real_subtotal - budget_subtotal,
                    })
                else:
                    class_items.append({
                        'number': subclass.number[:2],
                        'title': subclass.title,
                        'items': items,
                        'left_subtotal': real_subtotal,
                        'right_subtotal': budget_subtotal,
                        'diff_subtotal': budget_subtotal - real_subtotal,
                    })
                budget_total += budget_subtotal
                real_total += real_subtotal
                context[class_title] = class_items
                if form.cleaned_data['comparison'] == '1':
                    context[f'left_{class_title}_total'] = budget_total
                    context[f'right_{class_title}_total'] = real_total
                    context[f'diff_{class_title}_total'] = real_total - budget_total
                else:
                    context[f'left_{class_title}_total'] = real_total
                    context[f'right_{class_title}_total'] = budget_total
                    context[f'diff_{class_title}_total'] = budget_total - real_total
            budget_balance += budget_total * sign
            real_balance += real_total * sign
        context['budget'] = budget
        if form.cleaned_data['comparison'] == '1':
            context['left_title'] = f'Prévi.\n{budget.title}'
            context['right_title'] = f'Réal.\n{self.year.title}'
            context['left_balance'] = budget_balance
            context['right_balance'] = real_balance
            context['diff_balance'] = real_balance - budget_balance
            context['left_balance_neg'] = -budget_balance
            context['right_balance_neg'] = -real_balance
            context['diff_balance_neg'] = budget_balance - real_balance
        else:
            context['left_title'] = f'Réal.\n{self.year.title}'
            context['right_title'] = f'Prévi.\n{budget.title}'
            context['left_balance'] = real_balance
            context['right_balance'] = budget_balance
            context['diff_balance'] = budget_balance - real_balance
            context['left_balance_neg'] = -real_balance
            context['right_balance_neg'] = -budget_balance
            context['diff_balance_neg'] = real_balance - budget_balance
        context['analytic'] = analytic
        return context


class BudgetAccountView(PermissionRequiredMixin, ReadYearMixin, DetailView):
    template_name = "accounting/budget_account.html"
    model = Account
    permission_required = 'accounting.view_budgetentry'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = BudgetFilterForm(self.request.GET)
        context['form'] = form
        if not form.is_valid():
            return context
        budget = form.cleaned_data['budget']
        if self.request.GET.get('analytic'):
            analytic = get_object_or_404(Analytic, pk=self.request.GET['analytic'])
            analytic_q = Q(analytic=analytic)
        else:
            analytic = None
            analytic_q = Q()
        context['budget'] = budget
        context['items'] = BudgetEntry.objects.filter(
            analytic_q, budget=budget, account_id=self.kwargs['pk']
        ).order_by(
            '-amount'
        )
        context['items_total'] = context['items'].aggregate(total=Sum('amount'))['total']
        sign = -1 if self.object.number[0] == '6' else 1
        context['transactions'] = Transaction.objects.filter(
            analytic_q, account_id=self.kwargs['pk'], entry__year=self.year,
        ).annotate(
            qs_balance=(F('revenue') - F('expense')) * Value(sign)
        ).order_by(
            '-qs_balance',
        )
        context['transactions_total'] = context['transactions'].aggregate(total=Sum('qs_balance'))['total']
        context['analytic'] = analytic
        return context


class BudgetEntryCreateView(PermissionRequiredMixin, WriteYearMixin, CreateView):
    model = BudgetEntry
    form_class = BudgetEntryForm
    permission_required = 'accounting.add_budgetentry'

    def get_success_url(self):
        return reverse('budget-account', args=[self.year.pk, self.object.account.pk]) + \
            f'?budget={self.object.budget.pk}&analytic={self.object.analytic.pk if self.object.analytic else ""}'

    def get_initial(self):
        initial = {}
        if self.request.GET.get('budget'):
            initial['budget'] = get_object_or_404(Budget, pk=self.request.GET['budget'])
        if self.request.GET.get('analytic'):
            initial['analytic'] = get_object_or_404(Analytic, pk=self.request.GET['analytic'])
        if self.request.GET.get('account'):
            initial['account'] = get_object_or_404(Account, pk=self.request.GET['account'])
        return initial


class BudgetEntryUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    model = BudgetEntry
    form_class = BudgetEntryForm
    permission_required = 'accounting.change_budgetentry'

    def get_success_url(self):
        return reverse('budget', args=[self.year.pk])


class BudgetEntryDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = BudgetEntry
    permission_required = 'accounting.delete_budgetentry'

    def get_success_url(self):
        return reverse('budget-account', args=[self.year.pk, self.object.account.pk]) + \
            f'?budget={self.object.budget.pk}&analytic={self.object.analytic.pk if self.object.analytic else ""}'


class BudgetDeleteView(PermissionRequiredMixin, WriteYearMixin, DeleteView):
    model = Budget
    permission_required = 'accounting.delete_budget'

    def form_valid(self, form):
        self.object.budgetentry_set.all().delete()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('budget', args=[self.year.pk])


class BudgetUpdateView(PermissionRequiredMixin, WriteYearMixin, UpdateView):
    model = Budget
    fields = ('title', )
    permission_required = 'accounting.change_budget'

    def get_success_url(self):
        return reverse('budget', args=[self.year.pk]) + f'?budget={self.object.pk}'


class DepreciationsUpdateView(PermissionRequiredMixin, WriteYearMixin, SingleObjectMixin, TemplateView):
    template_name = 'accounting/depreciations_form.html'
    model = Entry
    permission_required = 'accounting.change_entry'

    def get_object(self):
        try:
            return Entry.objects.get(year=self.year, title="Dotation aux amortissements")
        except Entry.DoesNotExist:
            return Entry(
                year=self.year,
                title="Dotation aux amortissements",
                journal=Journal.objects.get(number='OD'),
                date=self.year.end,
            )

    def get_context_data(self, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = DepreciationForm(self.year, instance=self.object)
        if 'formset' not in kwargs:
            kwargs['formset'] = DepreciationFormSet(instance=self.object)
        return super().get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = DepreciationForm(self.year, instance=self.object, data=self.request.POST, files=self.request.FILES)
        formset = DepreciationFormSet(instance=self.object, data=self.request.POST, files=self.request.FILES)
        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            if 'add-row' in self.request.POST:
                return HttpResponseRedirect(reverse_lazy('depreciations', args=[self.year.pk]))
            else:
                return HttpResponseRedirect(reverse_lazy('entry_list', args=[self.year.pk]))
        else:
            print(f"{form.errors=}")
            print(f"{formset.errors=}")
            return self.render_to_response(self.get_context_data(form=form, formset=formset))
