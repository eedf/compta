# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import Year, Account, Analytic, ThirdParty
from .serializers_v1 import YearSerializer1, AccountSerializer1, AnalyticSerializer1, ThirdPartySerializer1


class APIPermission(BasePermission):
    def has_permission(self, request, view):
        meta = view.queryset.model._meta
        if request.method in SAFE_METHODS:
            return request.user.has_perm('{}.apiv1_read_{}'.format(meta.app_label, meta.model_name))
        else:
            return request.user.has_perm('{}.apiv1_write_{}'.format(meta.app_label, meta.model_name))


class YearViewSet1(ReadOnlyModelViewSet):
    """Exercices comptables."""

    lookup_field = 'uuid'
    queryset = Year.objects.order_by('id')
    serializer_class = YearSerializer1
    permission_classes = [APIPermission]


class AccountViewSet1(ReadOnlyModelViewSet):
    """Comptes généraux."""

    lookup_field = 'uuid'
    queryset = Account.objects.order_by('id')
    serializer_class = AccountSerializer1
    permission_classes = [APIPermission]


class AnalyticViewSet1(ReadOnlyModelViewSet):
    """Comptes analytiques."""

    lookup_field = 'uuid'
    queryset = Analytic.objects.order_by('id')
    serializer_class = AnalyticSerializer1
    permission_classes = [APIPermission]


class ThirdPartyViewSet1(ReadOnlyModelViewSet):
    """Tiers."""

    lookup_field = 'uuid'
    queryset = ThirdParty.objects.order_by('id')
    serializer_class = ThirdPartySerializer1
    permission_classes = [APIPermission]
