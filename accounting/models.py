# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import datetime
from decimal import Decimal
from uuid import uuid4
from django.db import models
from django.db.models.functions import Coalesce
from django.core.validators import MinValueValidator
from django.urls import reverse
from localflavor.generic.models import IBANField, BICField
from members.models import Person


class Year(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    start = models.DateField(verbose_name="Début")
    end = models.DateField(verbose_name="Fin")
    opened = models.BooleanField(verbose_name="Ouvert", default=False)

    class Meta:
        verbose_name = "Exercice"
        ordering = ('start', 'end')
        permissions = [
            ('apiv1_read_year', "Can read Year with API v1"),
        ]

    def __str__(self):
        return self.title


class Journal(models.Model):
    number = models.CharField(verbose_name="Numéro", max_length=3, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)

    class Meta:
        verbose_name = "Journal"
        ordering = ('number', )

    def __str__(self):
        return "{} : {}".format(self.number, self.title)


class Account(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    number = models.CharField(verbose_name="Numéro", max_length=7, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    enable_in_reports = models.BooleanField(verbose_name="NdF", help_text="Activer dans les notes de frais",
                                            default=False)

    class Meta:
        verbose_name = "Compte"
        ordering = ('number', )
        permissions = [
            ('apiv1_read_account', "Can read Account with API v1"),
        ]

    def __str__(self):
        return "{} : {}".format(self.number, self.title)


class ThirdParty(models.Model):
    TYPE_CHOICES = (
        (0, "Client"),
        (1, "Fournisseur"),
        (2, "Salarié"),
        (3, "Autre"),
    )

    uuid = models.UUIDField(unique=True, default=uuid4)
    number = models.CharField(verbose_name="Numéro", max_length=4, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    iban = IBANField(verbose_name="IBAN", blank=True)
    bic = BICField(verbose_name="BIC", blank=True)
    client_number = models.CharField(verbose_name="Numéro client", max_length=100, blank=True)
    account = models.ForeignKey(Account, verbose_name="Compte principal", on_delete=models.PROTECT)
    type = models.IntegerField(verbose_name="Type", choices=TYPE_CHOICES)
    sage = models.CharField(verbose_name="Numéro SAGE", max_length=16, unique=True, blank=True, null=True)
    address = models.TextField(verbose_name="Adresse postale", blank=True)

    class Meta:
        verbose_name = "Tiers"
        verbose_name_plural = "Tiers"
        ordering = ('number', )
        permissions = [
            ('apiv1_read_thirdparty', "Can read ThirdParty with API v1"),
        ]

    def __str__(self):
        return "{} : {}".format(self.number, self.title)

    @property
    def account_number(self):
        return self.account.number

    @property
    def sage_number(self):
        return self.sage or self.number


class Analytic(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    number = models.CharField(verbose_name="Numéro", max_length=3, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)

    class Meta:
        verbose_name = "Analytique"
        ordering = ('number', )
        permissions = [
            ('apiv1_read_analytic', "Can read Analytic with API v1"),
        ]

    def __str__(self):
        return "{} : {}".format(self.number, self.title)


class EntryManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.annotate(
            revenue=Coalesce(models.Sum('transaction__revenue'), Decimal(0)),
            expense=Coalesce(models.Sum('transaction__expense'), Decimal(0)),
            balance=Coalesce(models.Sum('transaction__revenue') - models.Sum('transaction__expense'), Decimal(0))
        )
        return qs


class Entry(models.Model):
    year = models.ForeignKey(Year, verbose_name="Exercice", on_delete=models.PROTECT)
    date = models.DateField(verbose_name="Date", default=datetime.date.today)
    journal = models.ForeignKey(Journal, verbose_name="Journal", on_delete=models.PROTECT)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    scan = models.FileField(verbose_name="Justificatif", upload_to='justificatif', blank=True)
    exported = models.BooleanField(verbose_name="Exporté", default=False)

    objects = EntryManager()

    class Meta:
        verbose_name = "Écriture"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('entry', kwargs={'pk': self.pk})

    def balanced(self):
        return self.balance == 0
    balanced.short_description = "Équilibré"
    balanced.boolean = True

    def delete(self, *args, **kwargs):
        "Delete lettering if need be"
        Letter.objects.filter(transaction__entry=self).delete()
        return super().delete(*args, **kwargs)

    @property
    def thirdparties(self):
        return (transaction.thirdparty for transaction in self.transaction_set.exclude(thirdparty=None))


class Purchase(Entry):
    deadline = models.DateField(verbose_name="Date limite", null=True, blank=True)
    number = models.CharField(verbose_name="Numéro", max_length=100, blank=True)

    objects = EntryManager()

    class Meta:
        verbose_name = "Facture fournisseur"
        verbose_name_plural = "Factures fournisseur"

    @property
    def provider_transaction(self):
        try:
            return self.transaction_set.exclude(account__number='4090000').get(account__number__startswith='4')
        except Transaction.DoesNotExist:
            return None


class Sale(Entry):
    number = models.CharField(verbose_name="Numéro", max_length=100, blank=True)
    order = models.CharField(verbose_name="Bon de commande", max_length=100, blank=True)

    objects = EntryManager()

    class Meta:
        verbose_name = "Facture client"
        verbose_name_plural = "Factures client"

    @property
    def client_transaction(self):
        try:
            return self.transaction_set.exclude(account__number='4190000').get(account__number__startswith='4')
        except Transaction.DoesNotExist:
            return None


class Income(Entry):
    objects = EntryManager()
    METHOD_CHOICES = (
        ('5112000', "Chèque"),
        ('5115000', "ANCV"),
        ('5120000', "Virement"),
        ('5170000', "Carte"),
        ('5300000', "Espèces"),
    )

    class Meta:
        verbose_name = "Recette"
        verbose_name_plural = "Recettes"

    @property
    def client_transaction(self):
        try:
            return self.transaction_set.get(account__number__startswith='4')
        except Transaction.DoesNotExist:
            return None

    @property
    def cash_transaction(self):
        try:
            return self.transaction_set.get(account__number__startswith='5')
        except Transaction.DoesNotExist:
            return None

    @property
    def deposit(self):
        if not self.client_transaction:
            return None
        return self.client_transaction.account.number == '4190000'

    @property
    def method(self):
        if not self.cash_transaction:
            return None
        if self.cash_transaction.account.number.startswith('512'):
            return dict(self.METHOD_CHOICES)['5120000']
        return dict(self.METHOD_CHOICES)[self.cash_transaction.account.number]


class Expenditure(Entry):
    METHOD_CHOICES = (
        (1, "Carte bancaire"),
        (2, "Chèque"),
        (3, "Espèces"),
        (4, "Prélèvement"),
        (5, "Virement"),
    )

    method = models.IntegerField(choices=METHOD_CHOICES)

    objects = EntryManager()

    class Meta:
        verbose_name = "Dépense"
        verbose_name_plural = "Dépenses"

    @property
    def provider_transactions(self):
        return self.transaction_set.filter(account__number__startswith='4')

    @property
    def cash_transaction(self):
        try:
            return self.transaction_set.get(account__number__startswith='5')
        except Transaction.DoesNotExist:
            return None


class Cashing(Entry):
    METHOD_CHOICES = (
        ('5112000', "Chèques"),
        ('5115000', "ANCV"),
        ('5170000', "Carte"),
        ('5300000', "Espèces"),
    )

    objects = EntryManager()

    class Meta:
        verbose_name = "Remise"
        verbose_name_plural = "Remises"

    @property
    def bank_transaction(self):
        try:
            return self.transaction_set.get(account__number__in=('5120000', '5122000'))
        except Transaction.DoesNotExist:
            return None

    @property
    def cashing_transaction(self):
        try:
            return self.transaction_set.exclude(account__number__in=('5120000', '5122000', '6270000')).get()
        except Transaction.DoesNotExist:
            return None

    @property
    def fees_transaction(self):
        try:
            return self.transaction_set.get(account__number='6270000')
        except Transaction.DoesNotExist:
            return None

    @property
    def method(self):
        return dict(self.METHOD_CHOICES)[self.cashing_transaction.account.number]


class Withdrawal(Entry):
    METHOD_CHOICES = (
        ('5300000', "Espèces"),
    )

    objects = EntryManager()

    class Meta:
        verbose_name = "Retrait"
        verbose_name_plural = "Retraits"

    @property
    def bank_transaction(self):
        try:
            return self.transaction_set.get(account__number__startswith='512')
        except Transaction.DoesNotExist:
            return None

    @property
    def withdrawal_transaction(self):
        try:
            return self.transaction_set.exclude(account__number__startswith='512').get()
        except Transaction.DoesNotExist:
            return None

    @property
    def method(self):
        return dict(self.METHOD_CHOICES)[self.withdrawal_transaction.account.number]


class Letter(models.Model):
    def __str__(self):
        i = self.id - 1
        s = ''
        while i:
            s = chr((i % 26) + 65) + s
            i //= 26
        return s

    class Meta:
        verbose_name = 'Lettrage'
        ordering = ('id', )


class TransactionQuerySet(models.QuerySet):
    def amount_expense(self):
        return sum((transaction.expense - transaction.revenue for transaction in self.all()))

    def amount_revenue(self):
        return sum((transaction.revenue - transaction.expense for transaction in self.all()))


class Transaction(models.Model):
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE)
    title = models.CharField(verbose_name="Intitulé", max_length=100, blank=True)
    account = models.ForeignKey(Account, verbose_name="Compte", on_delete=models.PROTECT)
    thirdparty = models.ForeignKey(ThirdParty, verbose_name="Tiers", null=True, blank=True,
                                   on_delete=models.PROTECT)
    analytic = models.ForeignKey(Analytic, verbose_name="Analytique", blank=True, null=True, on_delete=models.PROTECT)
    expense = models.DecimalField(verbose_name="Débit", max_digits=8, decimal_places=2, default=0,
                                  validators=[MinValueValidator(0)])
    revenue = models.DecimalField(verbose_name="Crédit", max_digits=8, decimal_places=2, default=0,
                                  validators=[MinValueValidator(0)])
    letter = models.ForeignKey(Letter, verbose_name="Lettrage", blank=True, null=True, on_delete=models.SET_NULL)

    objects = models.Manager.from_queryset(TransactionQuerySet)()

    class Meta:
        constraints = (
            models.CheckConstraint(check=models.Q(expense__gte=0), name='positive_expense'),
            models.CheckConstraint(check=models.Q(revenue__gte=0), name='positive_revenue'),
        )

    def __str__(self):
        return self.title or self.entry.title

    def date(self):
        return self.entry.date
    date.short_description = "Date"
    date.admin_order_field = "entry__date"

    @property
    def date_dmy(self):
        return self.date().strftime('%d%m%y')

    @property
    def balance(self):
        return self.revenue - self.expense

    @property
    def amount_revenue(self):
        return self.revenue - self.expense

    @property
    def amount_expense(self):
        return self.expense - self.revenue

    @property
    def account_number(self):
        return self.account.number

    @property
    def thirdparty_sage_number(self):
        return self.thirdparty and self.thirdparty.sage_number

    @property
    def journal_number(self):
        return self.entry.journal.number

    @property
    def full_title(self):
        return str(self)

    def save(self, *args, **kwargs):
        "Delete lettering if need be"
        if self.id and self.letter:
            try:
                old = Transaction.objects.get(id=self.id)
            except Transaction.DoesNotExist:
                old = None
            if old and self.expense != old.expense or self.revenue != old.revenue:
                self.letter.delete()
                self.letter = None
        return super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        "Delete lettering if need be"
        if self.letter:
            self.letter.delete()
        return super().delete(*args, **kwargs)


class ReportManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.annotate(
            expense=Coalesce(models.Sum('reportitem__expense'), Decimal(0))
        )
        return qs


class Report(models.Model):
    year = models.ForeignKey(Year, verbose_name="Exercice", on_delete=models.PROTECT)
    person = models.ForeignKey(Person, verbose_name="Personne", on_delete=models.PROTECT, related_name='reports')
    date = models.DateField(verbose_name="Date de l'achat", null=True)
    scan = models.FileField(verbose_name="Justificatif", upload_to='reports', null=True)
    title = models.CharField(verbose_name="Objet", max_length=100)
    signature_date = models.DateField(null=True, blank=True)
    treasurer = models.ForeignKey(Person, on_delete=models.PROTECT, null=True, blank=True,
                                  related_name='countersignatures')
    countersignature_date = models.DateField(null=True, blank=True)
    purchase = models.OneToOneField(Purchase, verbose_name="Facture fournisseur", null=True, blank=True,
                                    on_delete=models.PROTECT)

    objects = ReportManager()

    class Meta:
        verbose_name = "Note de frais"
        verbose_name_plural = "Notes de frais"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('report', kwargs={'pk': self.pk})


class ReportItem(models.Model):
    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, verbose_name="Compte", on_delete=models.PROTECT)
    expense = models.DecimalField(verbose_name="Débit", max_digits=8, decimal_places=2, default=0)


class BankOperationType(models.Model):
    title = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Type d'opération bancaire"
        verbose_name_plural = "Types d'opération bancaire"
        ordering = ('title', )

    def __str__(self):
        return self.title


class BankAccount(models.Model):
    title = models.CharField(max_length=100)
    iban = IBANField(verbose_name="IBAN", blank=True)
    account = models.ForeignKey(Account, verbose_name="Compte", on_delete=models.PROTECT)
    default = models.BooleanField(verbose_name="Défaut", default=False)
    journal = models.ForeignKey(Journal, verbose_name="Journal", on_delete=models.PROTECT, default=2)

    class Meta:
        verbose_name = "Compte bancaire"
        verbose_name_plural = "Comptes bancaires"
        ordering = ('-default', 'title')

    def __str__(self):
        return self.title


class BankOperation(models.Model):
    account = models.ForeignKey(BankAccount, verbose_name="Compte", on_delete=models.PROTECT)
    year = models.ForeignKey(Year, verbose_name="Exercice", on_delete=models.PROTECT)
    date = models.DateField()
    order = models.IntegerField(verbose_name="Numéro d'ordre (du jour)")
    reference = models.CharField(verbose_name="Référence", max_length=10)
    type = models.ForeignKey(BankOperationType, on_delete=models.PROTECT)
    amount = models.DecimalField(verbose_name="Montant", max_digits=8, decimal_places=2, default=0)
    details = models.TextField(verbose_name="Détails", blank=True)
    transaction = models.OneToOneField(Transaction, verbose_name="Écriture", null=True, blank=True,
                                       related_name="operation", on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Opération bancaire"
        verbose_name_plural = "Opérations bancaires"

    def __str__(self):
        return self.reference

    @property
    def detail1(self):
        return self.details.split("\n")[0]


class Budget(models.Model):
    title = models.CharField(verbose_name="Intitulé", max_length=100)

    class Meta:
        verbose_name = "Budget"

    def __str__(self):
        return self.title


class BudgetEntry(models.Model):
    budget = models.ForeignKey(Budget, verbose_name="Budget", on_delete=models.PROTECT)
    title = models.CharField(verbose_name="Intitulé", max_length=100, blank=True)
    account = models.ForeignKey(Account, verbose_name="Compte", on_delete=models.PROTECT)
    analytic = models.ForeignKey(Analytic, verbose_name="Analytique", blank=True, null=True, on_delete=models.PROTECT)
    amount = models.DecimalField(verbose_name="Montant", max_digits=8, decimal_places=2, default=0)

    class Meta:
        verbose_name = "Écriture budgétaire"
        verbose_name_plural = "Écritures budgétaires"

    def __str__(self):
        return self.title
