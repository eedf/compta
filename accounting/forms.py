# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML
from dal import autocomplete, forward
from django import forms
from django.core.validators import MinValueValidator
from django.db.models import Q
from .models import (Account, Journal, Purchase, ThirdParty, Transaction, Sale, Income, Expenditure, Cashing,
                     Withdrawal, Report, ReportItem, Letter, BankAccount, BudgetEntry, Analytic, Entry)


class PurchaseForm(forms.ModelForm):
    thirdparty = forms.ModelChoiceField(label="Fournisseur", queryset=ThirdParty.objects.all(),
                                        widget=autocomplete.ModelSelect2(url='thirdparty-autocomplete'))
    amount = forms.DecimalField(label="Montant total", max_digits=8, decimal_places=2)
    with_expenditure = forms.BooleanField(label="Paiement comptant", required=False)
    method = forms.ChoiceField(label="Moyen de paiement*", choices=Expenditure.METHOD_CHOICES, required=False)
    deposit = forms.DecimalField(label="Avance versée", max_digits=8, decimal_places=2, initial=0)
    account = forms.ModelChoiceField(label="Compte bancaire", queryset=BankAccount.objects.all(), empty_label=None)

    class Meta:
        model = Purchase
        fields = ('title', 'date', 'thirdparty', 'number', 'deadline', 'scan', 'amount', 'with_expenditure', 'method',
                  'deposit', 'account')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        purchase = kwargs.get('instance')
        if purchase:
            assert purchase.year == year
            assert purchase.journal.number == 'HA'
            try:
                self.deposit_transaction = purchase.transaction_set.get(account__number='4090000')
                self.fields['deposit'].initial = self.deposit_transaction.revenue
            except Transaction.DoesNotExist:
                self.deposit_transaction = None
            self.provider_transaction = purchase.provider_transaction
            self.fields['amount'].initial = 0
            if self.provider_transaction:
                self.fields['thirdparty'].initial = self.provider_transaction.thirdparty
                self.fields['amount'].initial += self.provider_transaction.amount_revenue
            if self.deposit_transaction:
                self.fields['thirdparty'].initial = self.deposit_transaction.thirdparty
                self.fields['amount'].initial += self.deposit_transaction.revenue
        else:
            self.deposit_transaction = None
            self.provider_transaction = None
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            HTML('<div class="row"><div class="col-md-4">'),
            'amount',
            HTML('</div><div class="col-md-4">'),
            'date',
            HTML('</div><div class="col-md-4">'),
            'deposit',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'thirdparty',
            HTML('</div><div class="col-md-4">'),
            'number',
            HTML('</div><div class="col-md-4">'),
            'deadline',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-12">'),
            'scan',
            HTML('</div></div>'),
        )
        if self.instance.pk:
            del self.fields['with_expenditure']
            del self.fields['method']
            del self.fields['account']
        else:
            self.helper.layout.extend([
                HTML('<div class="row"><div class="col-md-4">'),
                'with_expenditure',
                HTML('</div><div class="col-md-4">'),
                'method',
                HTML('</div><div class="col-md-4">'),
                'account',
                HTML('</div></div>'),
            ])

    def save(self):
        # Get values
        thirdparty = self.cleaned_data['thirdparty']
        amount = self.cleaned_data['amount']
        deposit = self.cleaned_data['deposit']
        with_expenditure = self.cleaned_data.get('with_expenditure')

        # Save letter
        if with_expenditure:
            letter = Letter.objects.create()
        else:
            letter = None

        # Save purchase entry
        purchase = super().save(commit=False)
        purchase.year = self.year
        purchase.journal = Journal.objects.get(number="HA")
        purchase.save()

        # Save provider transaction
        revenue = amount - deposit
        if deposit == 0 or revenue != 0:
            if not self.provider_transaction:
                self.provider_transaction = Transaction(entry=self.instance)
            self.provider_transaction.thirdparty = thirdparty
            self.provider_transaction.account = thirdparty.account
            self.provider_transaction.revenue = max(revenue, 0)
            self.provider_transaction.expense = max(-revenue, 0)
            if with_expenditure:
                self.provider_transaction.letter = letter
            self.provider_transaction.save()
        else:
            if self.provider_transaction:
                self.provider_transaction.delete()

        # Save deposit transaction
        if deposit != 0:
            if not self.deposit_transaction:
                account = Account.objects.get(number='4090000')
                self.deposit_transaction = Transaction(entry=self.instance, account=account)
            self.deposit_transaction.thirdparty = thirdparty
            self.deposit_transaction.revenue = deposit
            self.deposit_transaction.save()
        else:
            if self.deposit_transaction:
                self.deposit_transaction.delete()

        if with_expenditure:
            # Save expenditure
            method = self.cleaned_data['method']
            expenditure = Expenditure(
                year=self.year,
                date=purchase.date,
                method=method,
                title="Paiement {}".format(purchase.title),
            )
            if method == 3:
                expenditure.journal = Journal.objects.get(number="CA")
            else:
                expenditure.journal = self.cleaned_data['account'].journal
            expenditure.save()

            # Save expenditure transaction
            expenditure_transaction = Transaction(
                entry=expenditure,
                expense=revenue,
                thirdparty=thirdparty,
                account=thirdparty.account,
                letter=letter,
            )
            expenditure_transaction.save()

            # Save cash transaction
            cash_transaction = Transaction(
                entry=expenditure,
                revenue=revenue,
            )
            if method == 3:
                cash_transaction.account = Account.objects.get(number='5300000')
            else:
                cash_transaction.account = self.cleaned_data['account'].account
            cash_transaction.save()

        return purchase

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < self.year.start:
            raise forms.ValidationError("Date antérieure au début de l'exercice.")
        if date > self.year.end:
            raise forms.ValidationError("Date postérieure au début de l'exercice.")
        return date

    def clean(self):
        if self.cleaned_data.get('with_expenditure') and not self.cleaned_data['method']:
            self.add_error('method', "Ce champ est obligatoire.")


class PurchaseTransactionForm(forms.ModelForm):
    amount = forms.DecimalField()

    class Meta:
        model = Transaction
        fields = ('account', 'analytic', 'title', 'amount')
        widgets = {
            'account': autocomplete.ModelSelect2(url='account-autocomplete',
                                                 forward=[forward.Const('6,21', 'startswith')]),
            'analytic': autocomplete.ModelSelect2(url='analytic-autocomplete'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(
            Q(number__startswith='6') |
            Q(number__startswith='21')
        )
        if self.instance.pk:
            self.fields['amount'].initial = self.instance.amount_expense

    def clean(self):
        account = self.cleaned_data.get('account')
        analytic = self.cleaned_data.get('analytic')
        if account and account.number.startswith('6') and not analytic:
            raise forms.ValidationError("Le compte analytique doit être précisé pour les charges")
        if account and account.number.startswith('21') and analytic:
            raise forms.ValidationError("Le compte analytique ne doit pas être précisé pour les investissements")

    def save(self, commit=True):
        transaction = super().save(commit=False)
        transaction.expense = max(self.cleaned_data['amount'], 0)
        transaction.revenue = max(-self.cleaned_data['amount'], 0)
        if commit:
            transaction.save()


class PurchaseFormSet(forms.BaseInlineFormSet):
    def __new__(cls, *args, **kwargs):
        formset_class = forms.inlineformset_factory(
            Purchase,
            Transaction,
            form=PurchaseTransactionForm,
            extra=3,
        )
        formset = formset_class(
            *args,
            queryset=Transaction.objects.filter(
                Q(account__number__startswith='6') |
                Q(account__number__startswith='21')
            ),
            **kwargs
        )
        formset.helper = FormHelper()
        formset.helper.form_tag = False
        formset.helper.form_show_labels = False
        formset.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-3">'),
            'account',
            HTML('</div><div class="col-md-3">'),
            'analytic',
            HTML('</div><div class="col-md-3">'),
            'title',
            HTML('</div><div class="col-md-2">'),
            'amount',
            HTML('</div><div class="col-md-1" style="margin-top: 1ex;">'),
            HTML('<span class="glyphicon glyphicon-trash text-danger"></span>'),
            'DELETE',
            HTML('</div></div>'),
        )
        return formset


class SaleForm(forms.ModelForm):
    thirdparty = forms.ModelChoiceField(label="Client", queryset=ThirdParty.objects.all(),
                                        widget=autocomplete.ModelSelect2(url='thirdparty-autocomplete'))
    amount = forms.DecimalField(label="Montant total", max_digits=8, decimal_places=2)
    deposit = forms.DecimalField(label="Avance versée", max_digits=8, decimal_places=2, initial=0,
                                 validators=[MinValueValidator(0)])

    class Meta:
        model = Sale
        fields = ('title', 'date', 'thirdparty', 'number', 'scan', 'amount', 'order')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        sale = kwargs.get('instance')
        if sale:
            assert self.year == year
            assert sale.journal.number == 'VT'
            try:
                self.deposit_transaction = sale.transaction_set.get(account__number='4190000')
                self.fields['deposit'].initial = self.deposit_transaction.expense
            except Transaction.DoesNotExist:
                self.deposit_transaction = None
            self.client_transaction = sale.client_transaction
            self.fields['amount'].initial = 0
            if self.client_transaction:
                self.fields['thirdparty'].initial = self.client_transaction.thirdparty
                self.fields['amount'].initial += self.client_transaction.amount_expense
            if self.deposit_transaction:
                self.fields['thirdparty'].initial = self.deposit_transaction.thirdparty
                self.fields['amount'].initial += self.deposit_transaction.expense
        else:
            self.deposit_transaction = None
            self.client_transaction = None
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-8">'),
            'title',
            HTML('</div><div class="col-md-4">'),
            'date',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'amount',
            HTML('</div><div class="col-md-4">'),
            'deposit',
            HTML('</div><div class="col-md-4">'),
            'scan',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'thirdparty',
            HTML('</div><div class="col-md-4">'),
            'number',
            HTML('</div><div class="col-md-4">'),
            'order',
            HTML('</div></div>'),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < self.year.start:
            raise forms.ValidationError("Date antérieure au début de l'exercice.")
        if date > self.year.end:
            raise forms.ValidationError("Date postérieure au début de l'exercice.")
        return date

    def save(self):
        # Get values
        thirdparty = self.cleaned_data['thirdparty']
        amount = self.cleaned_data['amount']
        deposit = self.cleaned_data['deposit']

        # Save sale entry
        sale = super().save(commit=False)
        sale.year = self.year
        sale.journal = Journal.objects.get(number="VT")
        sale.save()

        # Save client transaction
        expense = amount - deposit
        if deposit == 0 or expense != 0:
            if not self.client_transaction:
                self.client_transaction = Transaction(entry=self.instance)
            self.client_transaction.thirdparty = thirdparty
            self.client_transaction.account = thirdparty.account
            self.client_transaction.expense = max(expense, 0)
            self.client_transaction.revenue = max(-expense, 0)
            self.client_transaction.save()
        else:
            if self.client_transaction:
                self.client_transaction.delete()

        # Save deposit transaction
        if deposit != 0:
            if not self.deposit_transaction:
                account = Account.objects.get(number='4190000')
                self.deposit_transaction = Transaction(entry=self.instance, account=account)
            self.deposit_transaction.thirdparty = thirdparty
            self.deposit_transaction.expense = deposit
            self.deposit_transaction.save()
        else:
            if self.deposit_transaction:
                self.deposit_transaction.delete()

        return sale


class SaleTransactionForm(forms.ModelForm):
    amount = forms.DecimalField()

    class Meta:
        model = Transaction
        fields = ('account', 'analytic', 'title', 'amount')
        widgets = {
            'account': autocomplete.ModelSelect2(url='account-autocomplete',
                                                 forward=[forward.Const('7', 'startswith')]),
            'analytic': autocomplete.ModelSelect2(url='analytic-autocomplete'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(number__startswith='7')
        self.fields['analytic'].required = True
        self.fields['amount'].initial = self.instance.amount_revenue

    def save(self, commit=True):
        transaction = super().save(commit=False)
        transaction.revenue = max(self.cleaned_data['amount'], 0)
        transaction.expense = max(-self.cleaned_data['amount'], 0)
        if commit:
            transaction.save()


class SaleFormSet(forms.BaseInlineFormSet):
    def __new__(cls, extra=3, initial=None, *args, **kwargs):
        formset_class = forms.inlineformset_factory(
            Purchase,
            Transaction,
            form=SaleTransactionForm,
            extra=(len(initial) if initial else 0) + extra,
        )
        formset = formset_class(
            *args,
            queryset=Transaction.objects.filter(account__number__startswith='7'),
            initial=initial,
            **kwargs
        )
        formset.helper = FormHelper()
        formset.helper.form_tag = False
        formset.helper.form_show_labels = False
        formset.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-3">'),
            'account',
            HTML('</div><div class="col-md-3">'),
            'analytic',
            HTML('</div><div class="col-md-3">'),
            'title',
            HTML('</div><div class="col-md-2">'),
            'amount',
            HTML('</div><div class="col-md-1" style="margin-top: 1ex;">'),
            HTML('<span class="glyphicon glyphicon-trash text-danger"></span>'),
            'DELETE',
            HTML('</div></div>'),
        )
        return formset


class IncomeForm(forms.ModelForm):
    thirdparty = forms.ModelChoiceField(label="Client", queryset=ThirdParty.objects.all(),
                                        widget=autocomplete.ModelSelect2(url='thirdparty-autocomplete'))
    amount = forms.DecimalField(label="Montant", max_digits=8, decimal_places=2)
    method = forms.ChoiceField(label="Moyen de paiement", choices=Income.METHOD_CHOICES)
    deposit = forms.BooleanField(label="Avance", required=False)
    account = forms.ModelChoiceField(label="Compte bancaire", queryset=BankAccount.objects.all(), empty_label=None)

    class Meta:
        model = Income
        fields = ('title', 'date', 'thirdparty', 'scan', 'amount', 'method', 'deposit', 'account')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        income = kwargs.get('instance')
        if income:
            assert self.year == year
            assert income.journal.number in ('BQ', 'BQ2', 'CA')
            self.fields['deposit'].initial = income.deposit
            if income.client_transaction:
                self.fields['thirdparty'].initial = income.client_transaction.thirdparty
                self.fields['amount'].initial = income.client_transaction.revenue
            if income.cash_transaction:
                if income.cash_transaction.account.number.startswith('512'):
                    self.fields['method'].initial = '5120000'
                    self.fields['account'].initial = BankAccount.objects.get(account=income.cash_transaction.account)
                else:
                    self.fields['method'].initial = income.cash_transaction.account.number
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-8">'),
            'title',
            HTML('</div><div class="col-md-4">'),
            'scan',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'amount',
            HTML('</div><div class="col-md-4">'),
            'date',
            HTML('</div><div class="col-md-4">'),
            'deposit',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'thirdparty',
            HTML('</div><div class="col-md-4">'),
            'method',
            HTML('</div><div class="col-md-4">'),
            'account',
            HTML('</div></div>'),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < self.year.start:
            raise forms.ValidationError("Date antérieure au début de l'exercice.")
        if date > self.year.end:
            raise forms.ValidationError("Date postérieure au début de l'exercice.")
        return date

    def save(self):
        # Get values
        thirdparty = self.cleaned_data['thirdparty']
        amount = self.cleaned_data['amount']
        deposit = self.cleaned_data['deposit']
        method = self.cleaned_data['method']
        account = self.cleaned_data['account']

        # Save income entry
        income = super().save(commit=False)
        income.year = self.year
        if method == '5300000':
            income.journal = Journal.objects.get(number="CA")
        elif method == '5120000':
            income.journal = account.journal
        else:
            income.journal = Journal.objects.get(number="BQ")
        income.save()

        # Save client transaction
        client_transaction = self.instance.client_transaction or Transaction(entry=self.instance)
        client_transaction.account = Account.objects.get(number='4190000') if deposit else thirdparty.account
        client_transaction.thirdparty = thirdparty
        client_transaction.revenue = amount
        client_transaction.save()

        # Save cash transaction
        cash_transaction = self.instance.cash_transaction or Transaction(entry=self.instance)
        if method == '5120000':
            cash_transaction.account = account.account
        else:
            cash_transaction.account = Account.objects.get(number=method)
        cash_transaction.expense = amount
        cash_transaction.save()

        return income


class ExpenditureForm(forms.ModelForm):
    method = forms.ChoiceField(label="Moyen de paiement", choices=Expenditure.METHOD_CHOICES)
    account = forms.ModelChoiceField(label="Compte bancaire", queryset=BankAccount.objects.all(), empty_label=None)

    class Meta:
        model = Expenditure
        fields = ('title', 'date', 'scan', 'method', 'account')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        expenditure = kwargs.get('instance')
        if expenditure:
            assert self.year == year
            assert expenditure.journal.number in ('BQ', 'BQ2', 'CA')
            if expenditure.method != 3:
                self.fields['account'].initial = BankAccount.objects.get(account=expenditure.cash_transaction.account)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-8">'),
            'title',
            HTML('</div><div class="col-md-4">'),
            'scan',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'date',
            HTML('</div><div class="col-md-4">'),
            'method',
            HTML('</div><div class="col-md-4">'),
            'account',
            HTML('</div></div>'),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < self.year.start:
            raise forms.ValidationError("Date antérieure au début de l'exercice.")
        if date > self.year.end:
            raise forms.ValidationError("Date postérieure au début de l'exercice.")
        return date

    def save(self, formset):
        # Save expenditure entry
        expenditure = super().save(commit=False)
        expenditure.year = self.year
        if expenditure.method == 3:
            expenditure.journal = Journal.objects.get(number="CA")
        else:
            expenditure.journal = self.cleaned_data['account'].journal
        expenditure.save()

        # Save cash transaction
        cash_transaction = self.instance.cash_transaction or Transaction(entry=self.instance)
        if expenditure.method == 3:
            cash_transaction.account = Account.objects.get(number='5300000')
        else:
            cash_transaction.account = self.cleaned_data['account'].account

        cash_transaction.revenue = sum([form.cleaned_data['expense'] for form in formset if form.cleaned_data])
        cash_transaction.save()

        return expenditure


class ExpenditureTransactionForm(forms.ModelForm):
    thirdparty = forms.ModelChoiceField(label="Fournisseur", queryset=ThirdParty.objects.all(),
                                        widget=autocomplete.ModelSelect2(url='thirdparty-autocomplete'))
    deposit = forms.BooleanField(label="Avance", required=False)

    class Meta:
        model = Transaction
        fields = ('thirdparty', 'title', 'expense', 'deposit')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            self.fields['deposit'].initial = self.instance.account.number == '4090000'
        except Account.DoesNotExist:
            self.fields['deposit'].initial = False

    def save(self, commit=True):
        transaction = super().save(commit=False)
        if self.cleaned_data['deposit']:
            transaction.account = Account.objects.get(number='4090000')
        else:
            transaction.account = transaction.thirdparty.account
        if commit:
            transaction.save()
        return transaction


class ExpenditureFormSet(forms.BaseInlineFormSet):
    def __new__(cls, *args, **kwargs):
        formset_class = forms.inlineformset_factory(
            Expenditure,
            Transaction,
            form=ExpenditureTransactionForm,
            extra=3,
        )
        formset = formset_class(
            *args,
            queryset=Transaction.objects.filter(account__number__startswith='4'),
            **kwargs
        )
        formset.helper = FormHelper()
        formset.helper.form_tag = False
        formset.helper.form_show_labels = False
        formset.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-3">'),
            'thirdparty',
            HTML('</div><div class="col-md-4">'),
            'title',
            HTML('</div><div class="col-md-2">'),
            'deposit',
            HTML('</div><div class="col-md-2">'),
            'expense',
            HTML('</div><div class="col-md-1" style="margin-top: 1ex;">'),
            HTML('<span class="glyphicon glyphicon-trash text-danger"></span>'),
            'DELETE',
            HTML('</div></div>'),
        )
        return formset


class CashingForm(forms.ModelForm):
    amount = forms.DecimalField(label="Montant", max_digits=8, decimal_places=2)
    fees = forms.DecimalField(label="Frais", max_digits=8, decimal_places=2, initial=0)
    method = forms.ChoiceField(label="Type de remise", choices=Cashing.METHOD_CHOICES)
    account = forms.ModelChoiceField(label="Compte bancaire", queryset=BankAccount.objects.all(), empty_label=None)

    class Meta:
        model = Cashing
        fields = ('title', 'date', 'scan', 'amount', 'fees', 'method', 'account')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        cashing = kwargs.get('instance')
        if cashing:
            assert self.year == year
            assert cashing.journal == BankAccount.objects.get(account=cashing.bank_transaction.account).journal
            balance = cashing.cashing_transaction.revenue - cashing.bank_transaction.expense
            if cashing.fees_transaction:
                self.fields['fees'].initial = cashing.fees_transaction.expense
                balance -= cashing.fees_transaction.expense
            else:
                self.fields['fees'].initial = 0
            assert balance == 0
            self.fields['amount'].initial = cashing.cashing_transaction.revenue
            self.fields['method'].initial = cashing.cashing_transaction.account.number
            self.fields['account'].initial = BankAccount.objects.get(account=cashing.bank_transaction.account)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            HTML('<div class="row"><div class="col-md-3">'),
            'amount',
            HTML('</div><div class="col-md-3">'),
            'fees',
            HTML('</div><div class="col-md-3">'),
            'method',
            HTML('</div><div class="col-md-3">'),
            'account',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'date',
            HTML('</div><div class="col-md-6">'),
            'scan',
            HTML('</div></div>'),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < self.year.start:
            raise forms.ValidationError("Date antérieure au début de l'exercice.")
        if date > self.year.end:
            raise forms.ValidationError("Date postérieure au début de l'exercice.")
        return date

    def save(self):
        # Get values
        amount = self.cleaned_data['amount']
        fees = self.cleaned_data['fees']
        method = self.cleaned_data['method']
        account = self.cleaned_data['account']

        # Save cashing entry
        cashing = super().save(commit=False)
        cashing.year = self.year
        cashing.journal = account.journal
        cashing.save()

        # Save cashing transaction
        cashing_transaction = self.instance.cashing_transaction or Transaction(entry=self.instance)
        cashing_transaction.account = Account.objects.get(number=method)
        cashing_transaction.revenue = amount
        cashing_transaction.save()

        # Save bank transaction
        bank_transaction = self.instance.bank_transaction or Transaction(entry=self.instance)
        bank_transaction.account = account.account
        bank_transaction.expense = amount - fees
        bank_transaction.save()

        # Save fees transaction
        if fees:
            fees_transaction = self.instance.fees_transaction or Transaction(entry=self.instance)
            fees_transaction.account = Account.objects.get(number='6270000')
            fees_transaction.expense = fees
            fees_transaction.save()
        elif self.instance.fees_transaction:
            self.instance.fees_transaction.delete()

        return cashing


class WithdrawalForm(forms.ModelForm):
    amount = forms.DecimalField(label="Montant", max_digits=8, decimal_places=2)
    # method = forms.ChoiceField(label="Type de retrait", choices=Withdrawal.METHOD_CHOICES)
    account = forms.ModelChoiceField(label="Compte bancaire", queryset=BankAccount.objects.all(), empty_label=None)

    class Meta:
        model = Withdrawal
        fields = ('title', 'date', 'scan', 'amount', 'account')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        withdrawal = kwargs.get('instance')
        if withdrawal:
            assert self.year == year
            assert withdrawal.withdrawal_transaction.account.number == '5300000'
            assert withdrawal.journal.number in ('BQ', 'BQ2')
            assert withdrawal.withdrawal_transaction.expense == withdrawal.bank_transaction.revenue
            self.fields['amount'].initial = withdrawal.withdrawal_transaction.expense
            self.fields['account'].initial = BankAccount.objects.get(account=withdrawal.bank_transaction.account)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            HTML('<div class="row"><div class="col-md-6">'),
            'amount',
            HTML('</div><div class="col-md-6">'),
            'account',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'date',
            HTML('</div><div class="col-md-6">'),
            'scan',
            HTML('</div></div>'),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < self.year.start:
            raise forms.ValidationError("Date antérieure au début de l'exercice.")
        if date > self.year.end:
            raise forms.ValidationError("Date postérieure au début de l'exercice.")
        return date

    def save(self):
        # Get values
        amount = self.cleaned_data['amount']
        account = self.cleaned_data['account']

        # Save withdrawal entry
        withdrawal = super().save(commit=False)
        withdrawal.year = self.year
        withdrawal.journal = account.journal
        withdrawal.save()

        # Save withdrawal transaction
        withdrawal_transaction = self.instance.withdrawal_transaction or Transaction(entry=self.instance)
        withdrawal_transaction.account = Account.objects.get(number='5300000')
        withdrawal_transaction.expense = amount
        withdrawal_transaction.save()

        # Save bank transaction
        bank_transaction = self.instance.bank_transaction or Transaction(entry=self.instance)
        bank_transaction.account = account.account
        bank_transaction.revenue = amount
        bank_transaction.save()

        return withdrawal


class ThirdPartyForm(forms.ModelForm):
    class Meta:
        model = ThirdParty
        fields = ('number', 'title', 'iban', 'bic', 'sage', 'account', 'type', 'address')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(
            Q(number__startswith='4') | Q(number__startswith='16'),
        ).exclude(number__endswith='00000')
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            HTML('<div class="row"><div class="col-md-6">'),
            'type',
            HTML('</div><div class="col-md-6">'),
            'account',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'number',
            HTML('</div><div class="col-md-6">'),
            'sage',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'iban',
            HTML('</div><div class="col-md-6">'),
            'bic',
            HTML('</div></div>'),
            'address',
        )


class AnalyticForm(forms.ModelForm):
    class Meta:
        model = Analytic
        fields = ('number', 'title')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-6">'),
            'title',
            HTML('</div><div class="col-md-6">'),
            'number',
            HTML('</div></div>'),
        )


class ReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ('title', 'date', 'scan', 'person')
        widgets = {
            'person': autocomplete.ModelSelect2(url='person_autocomplete'),
        }

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'person',
            'title',
            HTML('<div class="row"><div class="col-md-6">'),
            'date',
            HTML('</div><div class="col-md-6">'),
            'scan',
            HTML('</div></div>'),
        )
        if not user.has_perm('accounting.add_report'):
            del self.fields['person']
            self.helper.layout.pop(0)


class ReportItemForm(forms.ModelForm):
    class Meta:
        model = ReportItem
        fields = ('account', 'expense')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(
            enable_in_reports=True
        )


class ReportFormSet(forms.BaseInlineFormSet):
    def __new__(cls, *args, **kwargs):
        formset_class = forms.inlineformset_factory(
            Report,
            ReportItem,
            form=ReportItemForm,
            extra=3,
        )
        formset = formset_class(
            *args,
            **kwargs
        )
        formset.helper = FormHelper()
        formset.helper.form_tag = False
        formset.helper.form_show_labels = False
        formset.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-6">'),
            'account',
            HTML('</div><div class="col-md-5">'),
            'expense',
            HTML('</div><div class="col-md-1" style="margin-top: 1ex;">'),
            HTML('<span class="glyphicon glyphicon-trash text-danger"></span>'),
            'DELETE',
            HTML('</div></div>'),
        )
        return formset


class BudgetEntryForm(forms.ModelForm):
    class Meta:
        model = BudgetEntry
        fields = ('title', 'budget', 'account', 'analytic', 'amount')
        widgets = {
            'account': autocomplete.ModelSelect2(url='account-autocomplete',
                                                 forward=[forward.Const('6,7', 'startswith')]),
            'analytic': autocomplete.ModelSelect2(url='analytic-autocomplete'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(Q(number__startswith='6') | Q(number__startswith='7'))
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            'budget',
            'account',
            'analytic',
            'amount',
        )


class DepreciationForm(forms.ModelForm):
    amount = forms.DecimalField(label="Montant total", max_digits=8, decimal_places=2)

    class Meta:
        model = Purchase
        fields = ('amount', 'scan')

    def __init__(self, year, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = year
        entry = kwargs.get('instance')
        assert entry.year == year
        assert entry.journal.number == 'OD'
        assert entry.date == entry.year.end
        if entry.pk:
            self.expense_transaction = entry.transaction_set.get(account__number='6811000')
            self.fields['amount'].initial = self.expense_transaction.expense
        else:
            self.expense_transaction = None
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'amount',
            'scan',
        )

    def save(self):
        # Get values
        amount = self.cleaned_data['amount']

        # Save entry
        entry = super().save(commit=False)
        entry.save()

        # Save expense transaction
        if amount != 0:
            if not self.expense_transaction:
                self.expense_transaction = Transaction(entry=self.instance)
            self.expense_transaction.account = Account.objects.get(number='6811000')
            self.expense_transaction.expense = amount
            self.expense_transaction.save()
        else:
            if self.expense_transaction:
                self.expense_transaction.delete()

        return entry


class DepreciationTransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = ('account', 'revenue')
        widgets = {
            'account': autocomplete.ModelSelect2(url='account-autocomplete',
                                                 forward=[forward.Const('281', 'startswith')]),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Account.objects.filter(number__startswith='281')


class DepreciationFormSet(forms.BaseInlineFormSet):
    def __new__(cls, *args, **kwargs):
        formset_class = forms.inlineformset_factory(
            Entry,
            Transaction,
            form=DepreciationTransactionForm,
            extra=3,
        )
        formset = formset_class(
            *args,
            queryset=Transaction.objects.filter(account__number__startswith='281'),
            **kwargs
        )
        formset.helper = FormHelper()
        formset.helper.form_tag = False
        formset.helper.form_show_labels = False
        formset.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-6">'),
            'account',
            HTML('</div><div class="col-md-5">'),
            'revenue',
            HTML('</div><div class="col-md-1" style="margin-top: 1ex;">'),
            HTML('<span class="glyphicon glyphicon-trash text-danger"></span>'),
            'DELETE',
            HTML('</div></div>'),
        )
        return formset
