# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.apps import AppConfig


class AccountingConfig(AppConfig):
    name = 'accounting'
    verbose_name = 'Comptabilité'
    default_auto_field = 'django.db.models.BigAutoField'
