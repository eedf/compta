# Generated by Django 4.2 on 2024-03-21 06:24

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='account',
            options={'ordering': ('number',), 'permissions': [('apiv1_read_account', 'Can read Account with API v1')], 'verbose_name': 'Compte'},
        ),
        migrations.AlterModelOptions(
            name='analytic',
            options={'ordering': ('number',), 'permissions': [('apiv1_read_analytic', 'Can read Analytic with API v1')], 'verbose_name': 'Analytique'},
        ),
        migrations.AlterModelOptions(
            name='thirdparty',
            options={'ordering': ('number',), 'permissions': [('apiv1_read_thirdparty', 'Can read ThirdParty with API v1')], 'verbose_name': 'Tiers', 'verbose_name_plural': 'Tiers'},
        ),
        migrations.AlterModelOptions(
            name='year',
            options={'ordering': ('start', 'end'), 'permissions': [('apiv1_read_year', 'Can read Year with API v1')], 'verbose_name': 'Exercice'},
        ),
        migrations.AddField(
            model_name='account',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, null=True),
        ),
        migrations.AddField(
            model_name='analytic',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, null=True),
        ),
        migrations.AddField(
            model_name='thirdparty',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, null=True),
        ),
        migrations.AddField(
            model_name='year',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, null=True),
        ),
    ]
