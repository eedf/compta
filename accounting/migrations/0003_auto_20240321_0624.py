# Generated by Django 4.2 on 2024-03-21 06:24

from django.db import migrations
import uuid


def gen_uuid(apps, schema_editor):
    for model in ('Year', 'Account', 'Analytic', 'ThirdParty'):
        Model = apps.get_model('accounting', model)
        for row in Model.objects.all():
            row.uuid = uuid.uuid4()
            row.save(update_fields=["uuid"])


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0002_alter_account_options_alter_analytic_options_and_more'),
    ]

    operations = [
        migrations.RunPython(gen_uuid, reverse_code=migrations.RunPython.noop),
    ]
