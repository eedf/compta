# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Compta.
#
# Compta is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from rest_framework import serializers
from .models import Year, Account, Analytic, ThirdParty


class YearSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = (
            'uuid', 'id', 'title', 'start', 'end', 'opened',
        )


class AccountSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = (
            'uuid', 'id', 'number', 'title',
        )


class AnalyticSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Analytic
        fields = (
            'uuid', 'id', 'number', 'title',
        )


class ThirdPartySerializer1(serializers.ModelSerializer):
    class Meta:
        model = ThirdParty
        fields = (
            'uuid', 'id', 'number', 'title', 'type',
        )
