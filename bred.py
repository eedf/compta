#!/bin/env python3.11

from datetime import date, timedelta
import os
import stat
import requests
import sys
import tomllib
from pathlib import Path


with open(Path(__file__).parent / 'etc' / 'bred.toml', 'rb') as f:
    if stat.S_IMODE(os.fstat(f.fileno()).st_mode) & 0o077:
        raise Exception("Bad config file mode")
    config = tomllib.load(f)

LOGIN_URL = 'https://www.bred.fr/transactionnel/Authentication'
DOWNLOAD_URL = 'https://www.bred.fr/transactionnel/v2/services/applications/operations/download/' \
    '{account_number}/000/00/EUR/false/{begin}/{end}/0/9999/excelcsv/virgule/JJMMAAAA'
UPLOAD_URL = '/bankoperation/upload/{account_name}/'

credentials = {
    'trustedDevice': config['download']['device'],
    'identifiant': config['download']['username'],
    'password': config['download']['password'],
}
session = requests.Session()
response = session.post(LOGIN_URL, params=credentials)
assert response.status_code == 200
year = len(sys.argv) > 1 and int(sys.argv[1]) or date.today().year
begin = max((date.today() - timedelta(days=60)), date(year, 1, 1)).isoformat()
end = min(date.today(), date(year, 12, 31)).isoformat()
print(f"From {begin} to {end}")
for account_name, account_number in config['accounts'].items():
    print(f"Account {account_name} - {account_number}")
    download_url = DOWNLOAD_URL.format(account_number=account_number, begin=begin, end=end)
    download_response = session.get(download_url)
    if download_response.status_code != 200:
        print(f'{download_response.status_code=}')
        print(download_response.text)
        break
    upload_url = config['upload']['host'] + UPLOAD_URL.format(account_name=account_name)
    upload_response = requests.post(
        upload_url,
        data=download_response.content,
        allow_redirects=False,
        headers={'Authorization': f'token {config["upload"]["token"]}'}
    )
    if upload_response.status_code != 200:
        print(f'{upload_response.status_code=}')
        print(upload_response.text)
        break
